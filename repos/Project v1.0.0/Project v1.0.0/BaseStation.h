#pragma once
#include<itpp/itbase.h>
using namespace itpp;
#include <complex>
typedef std::complex<double> complex;

class CUser;

class CBaseStation
{
public:
	CBaseStation(void);
	~CBaseStation(void);

public:
	//*******************位置与基本信息参数*********************************
	complex  mLocation;                  //! 基站位置坐标
	unsigned mCellRadius;				 //! 小区半径
	int AdjanceCell[2];                  //!记录该基站所在小区的相邻小区
	//*********************分布式天线相关参数*******************************
	int gBSRAUNum;
	int mRAUAntennaNum;
	double mRAUSpacing;
	double mRAUAntennaSpacing;
	double mArrayBroadside;
	int a;
	int b;
	int c;
};
