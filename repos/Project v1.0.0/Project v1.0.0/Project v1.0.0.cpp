/*
//在这里添加你头文件的内容
#include "stdafx.h"
//#include<vld.h>
#include "itpp/itcomm.h"
using namespace itpp;
#include <fstream>
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <complex>
typedef std::complex<double> complex;
#include "GVariable.h"
#include"math.h"
#include"time.h"
#include"SystemLevelChannel.h"
#include "BaseStation.h"
#include "User.h"
extern double gIniTime;
*/

#ifndef HEAD_H
#define HEAD_H 1
//在这里添加你头文件的内容
#include "stdafx.h"
//#include<vld.h>
#include "itpp/itcomm.h"
using namespace itpp;
#include <fstream>
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <complex>
typedef std::complex<double> complex;
#include "GVariable.h"
#include"math.h"
#include"time.h"
#include"SystemLevelChannel.h"
#include "BaseStation.h"
#include "User.h"
extern double gIniTime;


#endif

#define PI 3.1415926
int TTINum;





void CellLayout(CBaseStation BaseStation[30][4])
{
	int i,j;
	double Real;
	double Imag=0;
	//纵坐标固定，横坐标变化

    int RAUNum=gBSNum*gBSRAUNum;
	
	BaseStation[0][0].mLocation.real(0);
	BaseStation[0][0].mLocation.imag(0);

	for(i=0;i< gBSNum;i++)
	{
	  for(j=0;j<gBSRAUNum;j++)
	  {
         Real=(i*gBSRAUNum+j)*gRAUSpacing;
		 BaseStation[i][j].mLocation.real(Real);
         BaseStation[i][j].mLocation.imag(0);
		 //		printf("%4d%4d%15.6lf%15.6lf\n",i,j,BaseStation[i][j].mLocation.real(),BaseStation[i][j].mLocation.imag());
	  }
	}
		
}




void MemoryAlloc(CUser& User)
{
	
			User.pLargeScaleChannel.set_size(3*gBSRAUNum);
			User.pLargeScaleChannel.zeros();
			User.mLSP=new double*[3*gBSRAUNum];
			User.pSmallScaleChannel=new cmat*[3*gBSRAUNum];
			User.pChannelMatrix=new cmat*[3*gBSRAUNum];
	
			for(int j=0;j<3*gBSRAUNum;j++)
			{
				User.mLSP[j]=new double[5];
				User.pSmallScaleChannel[j]=new cmat[20];
				User.pChannelMatrix[j]=new cmat[20];
				
				for(int k=0;k<20;k++)
				{
					User.pSmallScaleChannel[j][k].set_size(gUEAntennaNum,gRAUAntennaNum);
					User.pChannelMatrix[j][k].set_size(gUEAntennaNum,gRAUAntennaNum);
					User.pSmallScaleChannel[j][k].zeros();
					User.pChannelMatrix[j][k].zeros();
				
				}
			
			}

}



		   


void IniArrayBroadside(CBaseStation BaseStation[30][4],CUser& User)
//!初始化天线正交方向(幅度)，也是最大增益方向 ----------- -------------------------余彩虹
{
	
	for(int i=0;i<30;i++)
		for(int j=0;j<4;j++)

	{
		BaseStation[i][j].mArrayBroadside=(double)(-90.0*2*PI/360.0);
		
	}
	
		User.mArrayBroadside=(double)(randu()*2*PI);//服从均匀分布[0,2*PI]


}

void GeometryFading(CBaseStation BS[30][4],CUser& User)
//!计算服务小区以及与服务小区相邻的小区的所有RAU与用户之间的大尺度衰落pLargeScaleChannel
{
    CSystemLevelChannel channel(1);

	double GF;
	
	complex Velocity;
	complex C1(0,0),C2(2500,-30),C3(100,0);
	SBSMSParameter bmp3={2,0.5,C1,-90,2,0.5,C2,0,C3};


	User.pLargeScaleChannel.set_size(3*gBSRAUNum);


	int ii,jj,kk;
	for(kk=0;kk<=2;kk++)//对离用户最近的三个基站中的每个RAU计算LSP
		{
			   ii=User.mServingCellID+kk-1;
			   for(jj=0;jj<gBSRAUNum;jj++)
			   {

				Velocity.real(User.mSpeed*cos(User.mMovingDirection));
				Velocity.imag(User.mSpeed*sin(User.mMovingDirection));
				bmp3.RAUAntennaNum=gRAUAntennaNum;//!RAU天线数
				bmp3.RAUAntennaSpacing=gRAUAntennaSpacing;//RAU天线间隔0.5,4,10(lambta)
				bmp3.RAUPosition=BS[ii][jj].mLocation;//!第i个基站中的第k个RAU的坐标
				bmp3.RAUArrayBroadside=BS[ii][jj].mArrayBroadside;//!RAU天线的正交方向
				bmp3.MSAntennaNum=gUEAntennaNum;//!移动台天线数
				bmp3.MSAntennaSpacing=gUEAntennaSpacing;//移动台天线间隔0.5(lambta)
				bmp3.MSPosition=User.mLocation;//!移动台坐标
				bmp3.MSArrayBroadside=User.mArrayBroadside;//!移动台天线的正交方向服从U(0,2*PI)
				bmp3.MSVelocity=Velocity;//!移动台速度矢量

				channel.IniGeneralParater(bmp3);
				
			    GF=channel.GenGeometryFading();
				User.pLargeScaleChannel(kk*gBSRAUNum+jj)=GF;
				channel.GetLSP(User.mLSP[kk*gBSRAUNum+jj]);//以供小尺度信道使用
				User.mScenarioIndex[kk*gBSRAUNum+jj]=channel.mIndex;			   
			   
			   
			   
			   }

		

			}


	
}


void SmallScaleFading(CBaseStation BS[30][4],CUser& User)
//!更新用户的小尺度衰落信息，写入小尺度衰落矩阵，即更新User.pSmallScaleChannel,四维矩阵,小尺度信息只计算当天服务小区与用户的信息。——余彩虹
{
	CSystemLevelChannel channel;
	int SampleNum;
	int Path;
	int RAUAntennaNum;
	int MSAntennaNum;
	complex Velocity;
	SBSMSParameter bmp2;
	int ii,jj,t,n,u,s;
			
		
    //end
		//!第二步，找到该用户的服务小区ID以及在该小区中的服务RAU ID。
		int SerCellId=User.mServingCellID;
		int SerRAUId=User.mServingRAUID;

		int totalID=gBSRAUNum+SerRAUId; //计算用户的服务小区在1*（3*gBSRAUNum）向量中的位置
       //!第三步，初始化信道基本信息
		channel.mIndex=User.mScenarioIndex[totalID];
		switch(channel.mIndex)
		{
	    case 0:channel.mIsNLOS=0;break;
		case 1:
		default:channel.mIsNLOS=1;break;
		}
		Velocity.real(User.mSpeed*cos(User.mMovingDirection));
		Velocity.imag(User.mSpeed*sin(User.mMovingDirection));
		bmp2.RAUAntennaNum=gRAUAntennaNum;//!RAU天线数
		bmp2.RAUAntennaSpacing=gRAUAntennaSpacing;//RAU天线间隔0.5,4,10(lambta)
		bmp2.RAUPosition=BS[SerCellId][SerRAUId].mLocation;//!RAU坐标
		bmp2.RAUArrayBroadside=BS[SerCellId][SerRAUId].mArrayBroadside;//!RAU天线的正交方向
		bmp2.MSAntennaNum=gUEAntennaNum;//!移动台天线数
		bmp2.MSAntennaSpacing=gUEAntennaSpacing;//移动台天线间隔0.5(lambta)
		bmp2.MSPosition=User.mLocation;//!移动台坐标
		bmp2.MSArrayBroadside=User.mArrayBroadside;//!移动台天线的正交方向服从U(0,2*PI)
		bmp2.MSVelocity=Velocity;//!移动台速度矢量
	    //测试
		//complex C1(0,0),C2(50,50),C3(3,4);
	 //   SBSMSParameter bmp={2,0.5,C1,30,3,2,0.5,C2,0,C3};
		//end
		channel.IniGeneralParater(bmp2);
		channel.PutLSP(User.mLSP[totalID]);
		channel.mGF=User.pLargeScaleChannel(totalID);
		//for(int in=0;in<5;in++)
		//cout<<User[i].mLSP[SerCellId][in]<<endl;

		//!第四步，动态分配信道内存，产生信道系数
		for(int sector=0;sector<3*gBSRAUNum;sector++)
		{
			SampleNum=channel.mSampleNum;
			Path=channel.mPath;
			RAUAntennaNum=channel.mRAUAntennaNum;
			MSAntennaNum=channel.mMSAntennaNum;
			channel.mChannelCoefficients=new cmat*[SampleNum];
			for(ii=0;ii<SampleNum;ii++)
			{
				channel.mChannelCoefficients[ii]=new cmat[Path];	   
				for(jj=0;jj<Path;jj++)
				{
					channel.mChannelCoefficients[ii][jj].set_size(MSAntennaNum,RAUAntennaNum,false);
					channel.mChannelCoefficients[ii][jj].zeros();
				}
			} 
			//cout<<"User id="<<i<<endl;
			channel.GenSmallScaleChannelCoef();
			//cout<<"end!"<<endl;

			//!第五步，把大尺度和小尺度参数结合起来
			User.pDelayperPath.set_size(20);
			User.mPathNum = Path;

			for(t=0;t<1;t++)//只取第一个时刻
			
				for(n=0;n<Path;n++)
					
				{
					User.pDelayperPath(n) = (int)(channel.mDelay(n)/((gTS*10e7)+0.5));
				 
					//cout<<User[i].pDelayperPath(n) <<" "<<gTS<<" "<<channel.mDelay(n)<<endl;
					
					for(u=0;u<MSAntennaNum;u++)
						for(s=0;s<RAUAntennaNum;s++)
						{
							//cout<<channel.mChannelCoefficients[t][n]<<endl;
							User.pSmallScaleChannel[sector][n](u,s)=channel.mChannelCoefficients[t][n](u,s);
							//cout<<User[i].pLargeScaleChannel(SerCellId)<<endl;
							channel.mChannelCoefficients[t][n](u,s)*=complex(pow(10,-User.pLargeScaleChannel(sector)*0.05),0);
							User.pChannelMatrix[sector][n](u,s)=channel.mChannelCoefficients[t][n](u,s);
							

						}
				}
		}
		//释放内存
		for(t=0;t<SampleNum;t++)
		{
			if(channel.mChannelCoefficients[t]!=NULL)
				delete[]  channel.mChannelCoefficients[t];
		}
		if(channel.mChannelCoefficients!=NULL)
			delete[] channel.mChannelCoefficients;


}






double CalSpectrumEfficiency()
{
	double table[2][6]={{0.2763,0.2423,0.2323,0.2263,0.2233,0.2223},{0.3003,0.2653,0.2563,0.2493,0.2473,0.2463}};
	double overheadFactor;
	//double PBCH=264;//所占RE数
	//double SCH=288;//同步信道所占RE数
	//double RSoverHead;
	//double central=0.119; 
	//计算开销因子
	int index1,index2;
	if(gRAUAntennaNum==2)
		index1=0;
	if(gRAUAntennaNum==4)
		index1=1;
	switch(int(gBandWidth))
	{
	case 1:
		index2=0;
		break;
	case 3:
		index2=1;
		break;
	case 5:
		index2=2;
		break;
	case 10:
		index2=3;
		break;
	case 15:
		index2=4;
		break;	
	case 20:
		index2=5;
		break;			
	}
    overheadFactor=1-table[index1][index2];
	return overheadFactor;
	//overheadFactor=RSoverHead+(PBCH+SCH)+gSubBandNum*gRBperSubBand*12*14+2/
	//注意gBandWidth要定义
	//gTotalSimuTime总仿真时间要定义
	
}


int _tmain(int argc, _TCHAR* argv[])
{

	int counter=10;
	int j;
	j=1;
	RNG_reset((unsigned)time(NULL));
    srand((unsigned)time(NULL));
	cout<<"**********************LTE-Rel.8系统仿真平台***********************"<<endl;
	cout<<"请输入TTI数："<<endl;
	cin>>TTINum;

	int UserperCluster = 1;
	
	

	//横坐标是基站编号，纵坐标是RAU编号
	CBaseStation BaseStation[30][4];
	
	//用户数
	CUser User;

	User.mSpeed=gSpeedOfTrain;

	//小区的排布，包括每个蔟中每个小区的坐标确定，即确定BaseStation.mLocation。——刘子悦
	CellLayout(BaseStation);
	//相邻小区确定，包括参考蔟中每个小区的干扰小区确定，即确定BaseStation.AdjanceCell。这个究竟是做一个表把每个小区的相邻小区写死，还是每次动态查找呢？——刘子悦
//	AdjanceCellDetermination(BaseStation);
	//分配内存--余彩虹（打包成一个函数？？）
	MemoryAlloc(User);


	IniArrayBroadside(BaseStation,User);
	

	

	for(int i = 0;i<TTINum;i++)
	{   //更新用户位置

		
      User.mLocation.real(2000+100*i*0.001);
	  User.mLocation.imag(30);
    
				
		
        GeometryFading(BaseStation,User);
	    SmallScaleFading(BaseStation,User);
	
			//cout<<User[0].pChannelMatrix[1][0]<<endl;
			//cout<<User[0].pChannelMatrix[0][0]<<endl;
			//!将大尺度衰落与小尺度衰落结合到一起，成为用户完整信道特性，即更新User.pChannelMatrix——余彩虹
			//CompleteFadingMatrix(User);
			//!每个用户根据与服务扇区的信道特性选择预编码矩阵，即更新User.pPrecodingMatrix。——周恩治
			//	PrecodingSeletion(User);
			//!根据一定的检测算法确定检测矩阵，即更新User.pDetectionMatrix。——周恩治
			User.PowerAllocation();
		
			
			//cout<<"Precoding and Detection!"<<endl;
			User.PrecodingDetectionSelection();
			//!根据预编码矩阵与检测矩阵，估计每个用户每个子载波的SINR,即更新User.pSubCarrierSINR。——刘恒
			//cout<<"SINR Estimation"<<endl;
			User.SINREst();


			//!将每个子载波的SINR按照一定映射估计，映射成每RB等效的SINR，即更新User.mEffectiveSINR。——刘恒
			//cout<<"SINR Mapping"<<endl;
			
		
			//!根据每个RB的SINR值，为每个用户选择相应的MCS，即更新User.MCSID,User.mCodingRate,User.mModType。——周维曦
			//cout<<"MCS Selection!"<<endl;
			User.MCSSelection();
			
			
			User.InstantThrougputComputing();
			//!计算每个激活用户每个子载波的实际SINR,即更新User.pSubCarrierSINR。——刘恒
			//cout<<"Calculate Actual SINR!"<<endl;
		  
			//!统计吞吐量，即更新BaseStation.pInstantThrougput，BaseStation.pAverageThroughput。——刘恒
			User.InstantThrougputComputing();
			//切换
        if(j==counter){
        User.Handover();
        j=1;

        }

		else{
		j++;
		}

 }
	CalSpectrumEfficiency();

	

   
    //释放内存----------------------余彩虹

		
			for(int t=0;t<3*gBSRAUNum;t++)	
				  {
					if(User.pSmallScaleChannel[t]!=NULL)
						delete[]  User.pSmallScaleChannel[t];
					if(User.pChannelMatrix[t]!=NULL)
						delete[]  User.pChannelMatrix[t];
					if(User.mLSP[t]!=NULL)
						delete[]  User.mLSP[t];
				  }
			if(User.pSmallScaleChannel!=NULL)
				delete[] User.pSmallScaleChannel;
			if(User.pChannelMatrix!=NULL)
				delete[] User.pChannelMatrix;
			if(User.mLSP!=NULL)
				delete[] User.mLSP;

	//end
	double overheadFactor=CalSpectrumEfficiency();
	double aver=User.mActuralInstantThoughput*14*overheadFactor/gBandWidth/TTINum*gFFTLength;
	double aaa;
    cin>>aaa;
	return 0;
}
