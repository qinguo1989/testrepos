#pragma once

#define  gCarrierFreq 2
#define PI 3.14159
#define EXP 2.718281828459045 
#define SAMPLEINTERVAL 0.0000001 //1ms




//   0        1                          
//{RMaLOS, RMaNLOS} 

extern const double LargeScaleparameter[10][2] = {
//DS
/*miu  */{-7.49, -7.43},
/*sigma*/{0.55,  0.48},
//ASD
/*miu  */{0.90, 0.95},
/*sigma*/{0.38, 0.45},
//ASA
/*miu  */{1.52, 1.52},
/*sigma*/{0.24, 0.13},
//SF
/*miu  */{NULL, NULL},
/*sigma*/{4, 8},
//K
/*miu  */{7, NULL},
/*sigma*/{4, NULL}
};


extern const double CrossCorrparameter[10][2] = {
/*ASD vs  DS*/{0, -0.4},
/*ASA vs  DS*/{0, 0},
/*ASA vs  SF*/{0, 0},
/*ASD vs  SF*/{0, 0.6},
/*DS  vs  SF*/{-0.5, -0.5},
/*ASD vs ASA*/{0, 0},
/*ASD vs K*/{0, NULL},
/*ASA vs K*/{0, NULL},
/*DS vs    K*/{0, NULL},
/*SF vs    K*/{0, NULL}
};

extern double gIniTime=0;//此为初始时刻

extern const int Clusterparameter[2][2] = {
	/*Number of clusters        */{11,  10},
	/*Number of rays per cluster*/{20,  20}
};

extern const double Dcorparameter[5][2] = {
	/*DS */{50,  36},
	/*ASD*/{25,  30},
	/*ASA*/{35,  40},
	/*SF */{37,  120},
	/*K  */{40,  NULL}
};


//Delay,Power,AoD,AoA in Scenario RMa:LOS,i.e.mIndex=0
//ClusterNum=11
extern const double SmallScaleFadingparameters0[4][11] = {
	/*Delay*/{0, 35, 45, 65, 65, 110, 125, 125, 170, 170, 200}, 
	/*Power*/{0.0,-16.9, -16.8, -18.3, -21.2, -17.1, -19.7, -23.8, -22.9, -20.9, -21.9},
	/*AoA  */{0, 99, 95, 99, -106, 96, -103, 113, 110, 106, -108},
	/*AoD  */{0, 24, 23, 24, -25, -23, -25, 27, 27, 25, -16}
};


//Delay,Power,AoD,AoA in Scenario RMa:NLOS,i.e.mIndex=1
//ClusterNum=10
extern const double SmallScaleFadingparameters1[4][10] = {
	/*Delay*/{0, 0, 5, 5, 10, 15, 15, 55, 110, 220}, 
	/*Power*/{-4.9, -7.8, -3.0, -2.7, -2.6, -3.6, -2.7, -5.6, -7.3, -10.3},
	/*AoA  */{-46, 58, 0, 34, 34, 49, -34, 49, 56, 67},
	/*AoD  */{-12, 16, 0, 9, -9, 13, -9, 13, 15, 18}
};

extern const double ClusterASDparameter[2] = 
/*Cluster ASD*/{2,  2};

extern const double ClusterASAparameter[2] = 
/*Cluster    ASA*/{3,  3};

extern const double RAUAntennaGain_boresight=17.0;
extern const double BTPower[2]={46,49};
extern const double UTPower=24;