#pragma once
#include<itpp/itbase.h>
using namespace itpp;
#include <complex>
typedef std::complex<double> complex;
using std::cin;
using std::cout;
using std::endl;
class CBaseStation;


class CUser
{
public:
	CUser(void);
	~CUser(void);

public:
	//***********************用户基本信息**********************************
	unsigned mUserID;					//! 用户ID号
	unsigned mServingCellID;			//! 用户所属基站ID	
	unsigned mServingRAUID;             //! 用户所选RAUID
	unsigned mGeographicalCellID;		//! 地理位置上所属的小区
	complex mLocation;					//! 用户位置，包括横纵坐标，为一个复数值
	unsigned mAntennaNum;				//! 用户天线数
	double  mArrayBroadside;			//! 用户天线阵列的正交方向

	//**********************信道相关参数***********************************	
	double **mLSP;                      //! 大尺度互相关参数       
	int mScenarioIndex[12];
	double mAntennaHeight;				//! 天线的高度	
	unsigned GUserAntennaNum;			//! 用户天线数
	double mSpeed;						//! 用户的速度
	double mMovingDirection;			//! 用户的运动方向
	int mMinLargeId;					//! 最小大尺度衰减的编号
	vec pLargeScaleChannel;				//! 用户到3个基站的大尺度衰落矩阵，每个基站gBSRAUNum个RAU
	cmat **pSmallScaleChannel;			//! 用户到3个基站的小尺度衰落矩阵，四维矩阵，第一维3个基站RAU个数，第二维径数，以及每径t*r的矩阵
	int mPathNum;						//! 路径损耗
	cmat **pChannelMatrix;				//! 用户到3个基站的RAU的衰落矩阵
	vec pDelayperPath;					//! 每径对应的时延

	//**************************预编码与检测矩阵***********************************
	vec mPrecodingMatrixID;				//! 用户在每个子带上传输的最优预编码矩阵id
	cmat pPrecodingMatrix[10];			//! 用户在每个子带上传输的最优预编码矩阵，三维矩阵，每个子带分配一个预编码矩阵,10个子带
	cmat pDetectionMatrix[10];			//! 用户在每个子带上的检测矩阵，三维矩阵，每个子带计算一个用户检测矩阵，子带对应用户

	//**************************SINR计算相关参数与结果****************************
	mat pSubCarrierSignalPower;			//! 每个子载波对应的功率
	mat pSubCarrierInterefence;			//! 每个子载波的干扰
	mat pSubCarrierSINR;				//! 用户每个子载波的SINR
	cvec FFTtemp[2][4];
	double mEffectiveSINR;				//! 用户该用户等效的SINR
	int mLayerNum;
	mat pChannelEstErr;					//! 信道估计误差

	//*************************MCS选择相关参数***********************************
	unsigned mMCSID;					//! 用户的MCS
	mat pCodingRate;					//! 用户的码率
	mat pModType;						//! 用户的调制方式，QPSK，16QAM
	mat pBeta;							//! mmse中的BETA值

	//***********************性能统计相关参数****************************************
	double mInstantThrougput;			//! 用户的即时吞吐率
	mat pRequiredThoughput;				//! 每个子带期望的吞吐量
	double mAveThoughput;				//!平均吞吐量
	double mActuralInstantThoughput;	//!实际的每TTI的吞吐量

	//***************************切换相关参数*******************************************
    bool  mInterruption;                //!表明通信是否处于中断状态
    int  mHODelay;                      //!切换总延时
    int mHONum;                         //!切换总次数
    int mHOFailNum;                     //!切换失败次数
    vec pSlidingWindowQin;				//!判断Qin是否发生的滑动窗口
    vec pSlidingWindowQout;				//!判断Qout是否发生的滑动窗口
    int  mT310;                         //! T310计时器
    int mRLFTimer;                      //!每隔10ms，更新滑动窗口
    double  mHOM;                        //!切换余量
    int  mTTT;                          //! Time To Trigger
    int mL1Timer;                       //!进行时间统计，每40ms测量一次RSRP
    int mL1Counter;                     //!进行次数统计，每5次向L3上报RSRP
	bool mTrigger;						//判断是否需要进行切换触发检验
    int mTTTCounter[2];                  //!用于记录每个测量小区已满足A3条件的时间
    double mLastUpLinkSIR;               //!记录HARQ重传的信干比，用于简单合并
    int mHARQCounter;                   //!记录HARQ重传次数(用于切换)
    int mRLCCounter;                    //!记录RLC重传次数(用于切换)
    int mSMRTimer;                      //! 10ms重传一次，用于判断是否到达下次测量报告重传时间
    double mLastDownLinkSIR;             //!记录上次下行重传时的信噪比，用于简单合并
	int HOCMDTimer;                     //!用于判断是否到了切换命令重传时间
	int mConnectTimer;                  //!  用于判断是否连接到目标小区的计时器
	double mBeta;                        //! L3滤波器系数
	mat pL1RSRP;						 //!记录服务小区，及其相邻小区的RSRP值（L1）
	mat pL3RSRP;						//分布式天线下的切换
	//double mL3RSRP[3];                   //!记录服务小区，及其相邻小区的RSRP值（L3）
	double mQin;                         //! Qin事件门限
	double mQout;                        //! Qout事件门限
	int mUEStatus;                      //!车载台状态
	int mTargetCellID;					//!目标小区ID
	int mTargetRAUID;                   //!目标RAUID
	int mPrapareTimer;					//准备时间计时器
	bool firstR3Filter;                 //判断是否第一次进行层3滤波
	int mL3Timer;                       //第一版用于简化目的的计时器
	
	//***************************分布式 天线相关 *******************************************
	vec pSelectedAntennaID;                              //!选择的分布式天线的序号
	vec pAllocatedPower;                                 //!每根所选择的天线上应分配的功率


	int a;
	int b;
	int c;
public:
	//************************************切换相关函数**************************************
	void Handover();                          
	bool RLFDetection();
	bool HOTrigger();
	void SendHOReport();
	void MeasureRSRP();
	double UpLinkSIR();
	double ChaseCom(double,double);
	void HOPrePare();
	void ReceiveHOCMD();
	void ConnectTargetCell();
	void HOComplete();
	double DownLinkSIR();
	void ResetParaAfterRLF();
	void ResetParaAfterHO();
	//************************************其它函数*************************************************
	//! 计算用户与各个小区之间的大尺度衰落
	void CalLargeScaleChannel (); 
	//! 计算用户与各个小区之间的小尺度衰落
	void CalSmallScaleChannel (); 
	//! 预测每个子载波的SINR，并将其映射成一个等效的符号SINR
	void SINREst (); 
	//! SINR映射
	double SINRMapping(int RENum,int InitialIndex,double CodeRate,int ModType,double Beta,int LayerID);
	//! 选择满足BLER的MCS
	void MCSSelection ();
	//! MCS选择函数
	bool MCSDetermination(double CodeRate,int ModType,double SINR);
	//! 获得用户的预编码矩阵
	void PrecodingDetectionSelection();
	//! 用户即时吞吐量统计
	void InstantThrougputComputing();
	//! 天线选择和功率分配
	void PowerAllocation();
	//!计算每个子载波受到的相邻5对子载波间干扰
	double interfromothercarriers(int SubCarrierIndex,int layid);
};
