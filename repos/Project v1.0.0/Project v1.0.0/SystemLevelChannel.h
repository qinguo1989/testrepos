#pragma once


//约定，内部角度均角度制，即degree


typedef struct{
	//!基站基本参数集合
	int RAUAntennaNum;             //!RAU天线数
	double RAUAntennaSpacing;      //!RAU天线间隔0.5,4,10(lambta)
	complex RAUPosition;           //!RAU坐标
	double RAUArrayBroadside;      //!RAU天线的正交方向(最大增益方向）rad

	//!移动台基本参数集合 
	int MSAntennaNum;             //!移动台天线数
	double MSAntennaSpacing;      //!移动台天线间隔0.5(lambta)
	complex MSPosition;           //!移动台坐标
    double MSArrayBroadside;      //!移动台天线的正交方向服从U(0,2*PI)rad
	complex MSVelocity;           //!移动台速度矢量

}SBSMSParameter;


class CSystemLevelChannel
{

	public:
	CSystemLevelChannel(int sample=1):mSampleNum(sample)
	{   
	};//!构造函数
	~CSystemLevelChannel()
	{
	};//!析构函数

	//attribute
	int mIsNLOS;                 //!是否为非视距模型
	int mIndex;                  //!常数数组索引号(等效成场景索引号，此场景细分到视距/非视距情况)                           
	                             //!!!!!!!!!!!这里的考虑是外界提供mScenario,通过大尺度计算mIsNLOS，从而得到mIndex.小尺度只使用mIndex
	double mDistance;            //!RAU与MS距离 


	double mArgDistance;         //!RAU与MS距离方向
    int mSampleNum;              //!抽样点数，也是时刻t.在系统仿真中，一般取1
	cmat **mChannelCoefficients; //!四维，时刻mSampleNum，径数mPath，每时刻每径t*r个矩阵
	vec mDelay;                  //!各径的相对时延
	int mPath;                   //!径数
	int mRAUAntennaNum;           //!RAU天线数
	int mMSAntennaNum;           //!移动台天线数
	double mGF;

 

	//operater
	void DefaultRun();									//!使用默认参数的运行函数，用于测试
	int GetmIsNLOS();                                   //!获得视距非视距类型
	void IniGeneralParater(SBSMSParameter bmp);         //!设置基站和移动台参数，根据场景索引号初始化表A.
    double GenGeometryFading();                         //!产生几何衰落
	void GenSmallScaleChannelCoef();                    //!产生小尺度衰落系数
	void GetLSP(double lsp[5]);							//!获得大尺度互相关参数
	void PutLSP(double lsp[5]);							//!给大尺度互相关参数直接赋值



	private:

	//attribute

    //!基本的BS和MS参数集合
	//!基站基本参数集合
	double mRAUAntennaSpacing;      //!RAU天线间隔0.5,4,10(lambta)
	complex mRAUPosition;           //!RAU坐标
	double mRAUArrayBroadside;      //!RAU天线的正交方向(最大增益方向）deg


	//!移动台基本参数集合 
	double mMSAntennaSpacing;      //!移动台天线间隔0.5(lambta)
	complex mMSPosition;           //!移动台坐标
    double mMSArrayBroadside;      //!移动台天线的正交方向服从U(0,2*PI) deg
	complex mMSVelocity;           //!移动台速度矢量

	double mMSAngleLOS;            //!MS端视距到达角deg
	double mRAUAngleLOS;           //!RAU端视距离开角deg

    
	//!大尺度衰落环境的参数集合
	double mPathloss;                   //!路径损耗
	double mDS;                         //deg
	double mASA;                        //deg
	double mASD;                        //deg
	double mSF;                       
	double mK;
	double mRAUAntennaGain;

	double mLSP[5];


    //!小尺度衰落环境每个可分离径的参数集合
	int mSubPath;               //!子径20
	double mDoppler;            //!最大多普勒频移
	vec mPower;                 //!各径的功率
	vec fais;                   //!各径到达角deg
	vec FAIs;                   //!各径离开角deg
	mat mAOA;                   //!各径每子径的到达角deg
	mat mAOD;                   //!各径每子径的离开角deg
	mat mFAIvv;					//!各径每子径vv方向随机角deg
	mat mFAIvh;					//!各径每子径vh方向随机角deg
	mat mFAIhv;					//!各径每子径hv方向随机角deg
	mat mFAIhh;					//!各径每子径hh方向随机角deg


	//operater
    void GenLargeScaleChannelPathLoss();                                        //!产生大尺度路径损耗
	void GenCorrelatedLargeScaleParameter();                                    //!产生大尺度相关参数
	void GenAntennaGain(double theta);	                                        //!计算天线增益theta(deg)
	double min(double a,double b);                                              //!比较大小值
	double LSPauto(int index);                                                  //!计算大尺度参数自相关系数
	cvec CalculateGridPosition(int  index);                                     //!计算大尺度参数分布图的格点坐标
	mat MimoCorr(double anglespread,double angle,double d,int M,mat fais);		//!计算发送端或接收端的相关矩阵
 
};


