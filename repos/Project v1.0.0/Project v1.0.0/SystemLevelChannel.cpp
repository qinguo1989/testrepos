#include "stdafx.h"
#include <iostream> 
#include<fstream>
#include <complex> 
typedef std::complex<double> complex; 
#include <algorithm>
using std::random_shuffle;
#include <vector>
#include"math.h"
#include"time.h"
using std::cin;
using std::cout;
using std::endl;
#include "itpp/itcomm.h"
using namespace itpp;
#include"SystemLevelChannel.h"
#include"data_TableA1.h"
#include"GVariable.h"



//!使用默认参数的运行函数，用于测试
void CSystemLevelChannel::DefaultRun()
{
    double GF;

	int a1=4;													//!RAU天线数
	double a2=4;												//!RAU天线间隔4,10(lambta)
	complex a3(0,0);											//!RAU坐标
	double a4=-90*PI/180;										//!RAU天线的正交方向(最大增益方向）rad

	int b1=2;													//!移动台天线数
	double b2=0.5;												//!移动台天线间隔0.5(lambta)
	complex b3(2500,-30);										//!移动台坐标
	double b4=0*PI/180;											//!移动台天线的正交方向服从U(0,2*PI)rad
	complex b5(100,0);											//!移动台速度矢量

	SBSMSParameter bmp1={a1,a2,a3,a4,b1,b2,b3,b4,b5};

	IniGeneralParater(bmp1);									//!设置基站和移动台参数，根据场景索引号初始化表A.
	GF=GenGeometryFading();										//!产生几何衰落

	mChannelCoefficients=new cmat*[mSampleNum];					//!创建信道系数矩阵
	for(int i=0;i<mSampleNum;i++)
	{
		mChannelCoefficients[i]=new cmat[mPath];	   
		for(int j=0;j<mPath;j++)
		{
			mChannelCoefficients[i][j].set_size(mMSAntennaNum,mRAUAntennaNum);
			mChannelCoefficients[i][j].zeros();
		}
	}

	GenSmallScaleChannelCoef();									//!产生小尺度衰落系数

	for(int t=0;t<mSampleNum;t++)								//!把大尺度和小尺度参数结合起来
	{
		for(int n=0;n<mPath;n++)
		{
			//cout<<"mChannelCoefficients["<<t<<"]["<<n<<"]="<<endl<<mChannelCoefficients[t][n]<<endl;
			for(int u=0;u<mMSAntennaNum;u++)
			    for(int s=0;s<mRAUAntennaNum;s++)			
				{
					mChannelCoefficients[t][n](u,s)*=complex(pow(10,-0.1*GF),0);
				}
			//cout<<"mChannelCoefficients["<<t<<"]["<<n<<"]="<<endl<<mChannelCoefficients[t][n]<<endl;		
		}
	}

	for(int t=0;t<mSampleNum;t++)								//释放内存：delete mChannelCoefficients;
          {
			if(mChannelCoefficients[t]!=NULL)delete[]  mChannelCoefficients[t];
		  }
    if(mChannelCoefficients!=NULL)delete[] mChannelCoefficients;	

}


//!产生几何衰落
double CSystemLevelChannel::GenGeometryFading()
{
	GenLargeScaleChannelPathLoss();								 //!产生大尺度路径损耗
	GenCorrelatedLargeScaleParameter();							 //!产生大尺度相关参数
	
	double arg=mRAUArrayBroadside-mArgDistance;
	if(arg>180||arg<=-180)arg=360-arg;
	GenAntennaGain(arg);										 //!计算天线增益theta(deg)
	
	mGF=mPathloss+mSF-(mRAUAntennaGain+RAUAntennaGain_boresight);//-BTPower[mScenario][0]-UTPower[mScenario];
	//cout<<"mBSArrayBroadside"<<mBSArrayBroadside*180/PI<<" mArgDistance"<<mArgDistance*180/PI<<endl;
	//cout<<(mBSArrayBroadside-mArgDistance)*180/PI<<endl;
	//cout<<"mGF=mPathloss+mSF-(mBSAntennaGain+BSAntennaGain_boresight[mScenario])"<<endl;
	//cout<<mGF<<" "<<mPathloss<<" "<<mSF<<" "<<mBSAntennaGain<<" "<<BSAntennaGain_boresight[mScenario]<<endl;
	return mGF;
}


//!产生大尺度路径损耗
void CSystemLevelChannel::GenLargeScaleChannelPathLoss()
{

	double pathloss;
	double fc=gSystemCentreFrequency;
	double d=mDistance;

	//场景参数
	double hBS,hUT,W,h,dBPs;




	
	double h172;
	switch(mIndex)
	{			
	//RMaLOS
	case 0:
			//10 m < d < dBP (4)
			//dBP < d < 10 000 m
			h=5.0;
			W=20.0;
			hBS=35.0;
			hUT=1.5;
			h172=pow(h,1.72);
			dBPs=2*PI*hBS*hUT*(fc*1000000000.0)/300000000.0;//d_bps=4(hBS-1.0)(hUT-1.0)fc/c;hBS=10m,hUT=1.5m
			if(10<d&&d<dBPs) pathloss=20.0*log10(40*PI*d*fc/3)+min(0.03*h172,10)*log10(d)-min(0.044*h172,14.77)+0.002*log10(h)*d;
			else pathloss=20.0*log10(40*PI*d*fc/3)+min(0.03*h172,10)*log10(dBPs)-min(0.044*h172,14.77)+0.002*log10(h)*dBPs+40*log(d/dBPs);
			break;
	//RMaNLOS
	case 1:	
	default:
		//10 m < d < 5 000 m,
			h=5.0;
			W=20.0;
			hBS=35.0;
			hUT=1.5;
			pathloss=161.04-7.1*log10(W)+7.5*log10(h)-(24.37-3.7*(h/hBS)*(h/hBS))*log10(hBS)+(43.42-3.1*log10(hBS))*(log10(d)-3)+20*log10(fc)-(3.2*(log10(11.75*hUT))*(log10(11.75*hUT))-4.97);
            break;
	}
	mPathloss=pathloss;
}


//!产生大尺度相关参数
void CSystemLevelChannel::GenCorrelatedLargeScaleParameter()
{
	mat sqrtmCnn[2] = {	"0.9659,0,0,-0.2588,0;0,1.0000,0,0,0;0,0,1.0000,0,0;-0.2588,0,0,0.9659,0;0,0,0,0,1.0000",

						"0.9556,-0.1735,0.0000,-0.2384,0;-0.1735,0.9380,0.0000,0.3001,0;0.0000,0.0000,1.0000,0.0000,0;-0.2384,0.3001,0.0000,0.9237,0;0,0,0,0,1.0000"
						};
   mat LSP,LSPcor,C0;
   mat cnn[1] = {"1,0,0,0,0; 0,1,0,0,0; 0,0,1,0,0; 0,0,0,1,0; 0,0,0,0,1"
   };   
   switch(mIsNLOS)
   {
   case 0:
	    LSP.set_size(5,1,false);
		LSPcor.set_size(5,1,false);
		C0.set_size(5,5,false);
	   	LSP(0,0)=LSPauto(0);
		LSP(1,0)=LSPauto(1);
		LSP(2,0)=LSPauto(2);
		LSP(3,0)=LSPauto(3);
		LSP(4,0)=LSPauto(4);
  
		C0=sqrtmCnn[mIndex]; 
		//C0=cnn[0];

	    LSPcor=cnn[0]*LSP;
		mDS=LSPcor(0,0);
		mASD=LSPcor(1,0);
		mASA=LSPcor(2,0); 
		mSF=LSPcor(3,0);
		mK=LSPcor(4,0);	   
	    break;
   case 1:
   default:
	    LSP.set_size(4,1,false);
		LSPcor.set_size(4,1,false);
		C0.set_size(4,4,false);
	   	LSP(0,0)=LSPauto(0);
		LSP(1,0)=LSPauto(1);
		LSP(2,0)=LSPauto(2);
		LSP(3,0)=LSPauto(3);
		for(int i=0;i<4;i++)
			for(int j=0;j<4;j++)
			{
				C0(i,j)=sqrtmCnn[mIndex](i,j);
				//C0(i,j)=cnn[0](i,j);
			}
	    LSPcor=C0*LSP;
		mDS=LSPcor(0,0);
		mASD=LSPcor(1,0);
		mASA=LSPcor(2,0); 
		mSF=LSPcor(3,0);
		mK=-100000;//不存在 
	    break;
   }
   //cout<<LSPcor<<endl;
   //cout<<LSP<<endl;
   //transform  normal distributed random numbers to scenario specific distributions

	mDS=pow(10,mDS*LargeScaleparameter[1][mIndex]+LargeScaleparameter[0][mIndex]);
	mASD=pow(10,mASD*LargeScaleparameter[3][mIndex]+LargeScaleparameter[2][mIndex]);
	mASA=pow(10,mASA*LargeScaleparameter[5][mIndex]+LargeScaleparameter[4][mIndex]);
	mSF=mSF*LargeScaleparameter[7][mIndex];
	mK=mK*LargeScaleparameter[9][mIndex]+LargeScaleparameter[8][mIndex];

    mLSP[0]=mDS;
    mLSP[1]=mASD;
    mLSP[2]=mASA;
    mLSP[3]=mSF;
    mLSP[4]=mK;
}


//!计算天线增益theta(deg)
void CSystemLevelChannel::GenAntennaGain(double theta)

{
	double Am=20,theta3dB=70,Temp;
	
	Temp=12*(theta/theta3dB)*( theta/theta3dB);
	if(Temp>Am)Temp=Am;
	mRAUAntennaGain =-1* (Temp);
}


//!获得大尺度互相关参数
void CSystemLevelChannel::GetLSP(double lsp[5])
{
	for(int i=0;i<5;i++)
		lsp[i]=mLSP[i];
}


//!给大尺度互相关参数直接赋值
void CSystemLevelChannel::PutLSP(double lsp[5])
{
	mDS=lsp[0];
	mASD=lsp[1];
	mASA=lsp[2];
	mSF=lsp[3];
	mK=lsp[4];
}


void CSystemLevelChannel::GenSmallScaleChannelCoef()
{
	
	double ClusterASA=ClusterASDparameter[mIndex];
	double ClusterASD=ClusterASAparameter[mIndex];
	mDelay.set_size(mPath);
	mPower.set_size(mPath);
	//!获得每径时延/每径功率
	switch(mIndex)
	{
	case 0:
		mDelay=SmallScaleFadingparameters0[0][11];
		mPower=SmallScaleFadingparameters0[1][11];
		break;
	case 1:
		mDelay=SmallScaleFadingparameters1[0][10];
		mPower=SmallScaleFadingparameters0[1][10];
		break;
	}
	
	//!产生到达角和离开角
	//fais:AoA; FAIs:AoD
	vec fais(mPath),FAIs(mPath);
	
	switch(mIndex)
	{
	case 0:
		fais=SmallScaleFadingparameters0[2][11];
		FAIs=SmallScaleFadingparameters0[3][11];		
		break;
	case 1:
		fais=SmallScaleFadingparameters1[2][10];
		FAIs=SmallScaleFadingparameters1[3][10];
		break;
	}

	mAOA.set_size(mPath,mSubPath,false);
	mAOD.set_size(mPath,mSubPath,false);
	
	vec Alpha=("0.0447,-0.0047,0.1413,-0.1413,0.2492,-0.2492,0.3715,-0.3715,0.5129,-0.5129,0.6797,-0.6797,0.8844,-0.8844,1.1481,-1.1481,1.5195,-1.5195,2.1551,-2.1551");

	for(int n=0;n<mPath;n++)
	{
		for(int m=0;m<mSubPath;m++)
		{
			mAOA(n,m)=fais(n)+ClusterASA*Alpha(m);
			mAOD(n,m)=FAIs(n)+ClusterASD*Alpha(m);
		}
	}

	//!随机配对random coupling
	std::vector<double> RandShuffle1(20);
	std::vector<double> RandShuffle2(20);
	for(int n=0;n<mPath;n++)
	{
		for(int m=0;m<mSubPath;m++)
		{
			RandShuffle1[m]=mAOA(n,m);
			RandShuffle2[m]=mAOD(n,m);
		}
		random_shuffle(RandShuffle1.begin(),RandShuffle1.end());
		random_shuffle(RandShuffle2.begin(),RandShuffle2.end());
	    for(int m=0;m<mSubPath;m++)
		{
			mAOA(n,m)=RandShuffle1[m];
			mAOD(n,m)=RandShuffle2[m];
		}
	}

     mat fai,corrR,corrT,corrRT,hr;
	 cmat hh;
	 fai.set_size(1,mSubPath,false);
     corrR.set_size(mMSAntennaNum,mMSAntennaNum,false);
     corrT.set_size(mRAUAntennaNum,mRAUAntennaNum,false);
     corrRT.set_size(mRAUAntennaNum*mMSAntennaNum,mRAUAntennaNum*mMSAntennaNum);
	
	 //!产生信道系数矩阵
	 for(int t=0;t<mSampleNum;t++)
	 {
		for(int n=0;n<mPath;n++)
		{
			fai=mAOA(n);
			corrR=MimoCorr(ClusterASA*Alpha(18),fais(n),mMSAntennaSpacing,mMSAntennaNum,fai);
			fai=mAOD(n);
			corrT=MimoCorr(ClusterASD*Alpha(18),FAIs(n),mRAUAntennaSpacing,mRAUAntennaNum,fai);
			corrRT=kron(corrR,corrT);
			hr=transpose(chol(corrRT));
			//cout<<corrR<<endl;
			//cout<<corrT<<endl;
			//cout<<hr<<endl;

			hh=randn_c(mMSAntennaNum*mRAUAntennaNum,1);
			hh=hr*hh;
			hh*=mPower(n);
			for(int u=0;u<mMSAntennaNum;u++)
				for(int s=0;s<mRAUAntennaNum;s++)
				{
					mChannelCoefficients[t][n](u,s)=hh(mMSAntennaNum*s+u);
				}
		}
	 }
}
	 


//!计算发送端或接收端的相关矩阵
mat CSystemLevelChannel::MimoCorr(double anglespread,double angle,double d,int M,mat fais)//角度扩展,角度,天线间隔,天线个数deg
{
	int L=1000;
    double anglespread1=720;
    double c=0;
	mat p(1,L);p.zeros();//power
	mat fai(1,L);fai.zeros();//角度
	mat fai1(1,L);fai1.zeros();
	mat FAI(1,L);FAI.zeros();
	cmat matrix1(M,1);matrix1.zeros();
	cmat matrix2(1,M);matrix2.zeros();
	cmat correlation1(M,M);correlation1.zeros();
	cmat correlation2(M,M);correlation2.zeros();
	mat correlation(M,M);correlation.zeros();
	for(int m=0;m<L;m++)
	{
	    fai1(0,m)=angle-anglespread1+2*anglespread1*m/L;
		fai(0,m)=2*pi*fai1(0,m)/360;
		FAI(0,m)=d*std::sin(fai(0,m));
		p(0,m)=1/(anglespread*sqrt(2.0))*exp(-1*sqrt(2.0)*abs(fai1(0,m)-angle)/anglespread)*2*anglespread1/L;
		c=p(0,m)+c;
	}
	for(int m=0;m<L;m++)
	{
		for(int n=0;n<M;n++)
		{
			//matrix1(n,1)=exp(i*FAI(m)*2*pi*(n-1));
			double theta=fai(0,m)*2*pi*(n-1);
			matrix1(n,0)=complex(cos(theta),sin(theta));

		}
		matrix2=hermitian_transpose(matrix1);
		correlation1=matrix1*matrix2;
		correlation1*=p(0,m);
		correlation2=correlation1+correlation2;
	}
	for(int m=0;m<M;m++)
		for(int n=0;n<M;n++)
			correlation(m,n)=abs(correlation2(m,n))/c;
	return correlation;
}


//!产生视距或非视距
int CSystemLevelChannel::GetmIsNLOS()
{
	double PrLOS;
	double d=mDistance;
	
	if(d>10.0)
		PrLOS=exp(-1*(d-10.0)/1000.0);
	else PrLOS=1;

    double rrr;	 
	rrr=rand()/(double)RAND_MAX;

	mIsNLOS=rrr<PrLOS?0:1;
	
	return mIsNLOS;
	
}


//!寻找较小值
double CSystemLevelChannel::min(double a,double b)
{
	return a>b?a:b;
}


//!计算大尺度参数自相关系数
double CSystemLevelChannel::LSPauto(int index)
{
	double lspi,sum=0;

	cvec Sposition;
	double Dcor;
	double d;

	Sposition=CalculateGridPosition(index);
	Dcor=Dcorparameter[index][mIndex];

	for(int i=0;i<4;i++)
	{
		//if(index<3)
		//	lspi=pow(10,randn()*sigma+miu);
		//else 
		lspi=randn();
		complex Si;
		Si=Sposition(i);
		d=abs(mMSPosition-Si);
		sum=sum+lspi*exp(-1*d/Dcor);
	}
	return sum;
}

//!计算大尺度参数分布图的格点坐标
cvec CSystemLevelChannel::CalculateGridPosition(int index)
//需要知道场景mIndex，要计算的大尺度标识index,用户坐标mMSPosition,通过获取相关距离，计算格点
{
	cvec sposition(4);
	double Dcor;
	Dcor=Dcorparameter[index][mIndex];
	double x=mMSPosition.real();
	double y=mMSPosition.imag();

	x=floor(x/Dcor)*Dcor;
	y=floor((y+Dcor/2.0)/Dcor)*Dcor-Dcor/2;

	sposition(0)=complex(x,y);
	sposition(1)=complex(x+Dcor,y);
	sposition(2)=complex(x,y+Dcor);
	sposition(3)=complex(x+Dcor,y+Dcor);
	return sposition;
}


//!设置基站和移动台参数，根据场景索引号初始化表A.
void CSystemLevelChannel::IniGeneralParater(SBSMSParameter bmp)
{
	mRAUAntennaNum=bmp.RAUAntennaNum;							//!基站天线数
	mRAUAntennaSpacing=bmp.RAUAntennaSpacing;					//基站天线间隔0.5,4,10(lambta)
	mRAUPosition=bmp.RAUPosition;								//!基站坐标
	mRAUArrayBroadside=bmp.RAUArrayBroadside*180.0/PI;		//!扇区天线的正交方向

	mMSAntennaNum=bmp.MSAntennaNum;							//!移动台天线数
	mMSAntennaSpacing=bmp.MSAntennaSpacing;					//移动台天线间隔0.5(lambta)
	mMSPosition=bmp.MSPosition;								//!移动台坐标
    mMSArrayBroadside=bmp.MSArrayBroadside*180.0/PI;		//!移动台天线的正交方向服从U(0,2*PI)幅度制
	mMSVelocity=bmp.MSVelocity;								//!移动台速度矢量


	complex dis=mMSPosition-mRAUPosition;
	mDistance=abs(dis);										//!RAU与MS距离

	mArgDistance=arg(dis)*180.0/PI;							//!RAU与MS距离方向

    mRAUAngleLOS=mArgDistance-mRAUArrayBroadside;				//!BS端视距离开角deg
	while(mRAUAngleLOS>180)
	{mRAUAngleLOS=mRAUAngleLOS-360;}
	while(mRAUAngleLOS<=-180)
	{mRAUAngleLOS=mRAUAngleLOS+360;}

	if(mArgDistance>=0)										//!MS端视距到达角deg
	    mMSAngleLOS=mArgDistance-180;
	else
		mMSAngleLOS=mArgDistance+180;
	while(mMSAngleLOS>180){mMSAngleLOS=mMSAngleLOS-360;}
	while(mMSAngleLOS<=-180){mMSAngleLOS=mMSAngleLOS+360;}
	
    int index=GetmIsNLOS();									//generate the los/nlos factor
	mIndex=mIsNLOS;
	mPath=Clusterparameter[0][mIndex];
	mSubPath=Clusterparameter[1][mIndex];
	//cout<<"mBSAngleLOS="<<mBSAngleLOS*180/PI<<endl;
	//cout<<"mMSAngleLOS="<<mMSAngleLOS*180/PI<<endl;
}
