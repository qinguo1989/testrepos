#include"User.h"
#include"math.h"
#include"GVariable.h"
#include "itpp/itcomm.h"
using namespace itpp;

CUser::CUser(void)
{
	//***************************切换相关参数初始化****************************
	mInterruption=false;
	mHODelay=0;
	mHOFailNum=0;
	mT310=0;
	mHOM=0;
	mTTT=200;
	mL1Timer=0;
	mL1Counter=0;
	mTrigger=false;
	mLastUpLinkSIR=0;
	mHARQCounter=0;
	mRLCCounter=0;
	mLastDownLinkSIR=0;
	mConnectTimer=0;
	mBeta=0.5;
	mQin=-6;//-6db
	mQout=-8;//-8db
	mUEStatus=1;
	mPrapareTimer=0;
	mActuralInstantThoughput=0;
	pSlidingWindowQin.set_size(10);
	pSlidingWindowQout.set_size(20);
	pSlidingWindowQin.zeros();
	pSlidingWindowQout.zeros();
	pL1RSRP.set_size(3,5);
	pL1RSRP.zeros();
	mTTTCounter[0]=0;
	mTTTCounter[1]=0;
	firstR3Filter=true;
	pL3RSRP.set_size(3,gBSRAUNum);
	//***********************************************************************
    mLocation.real(2000);
	mLocation.imag(30);
	mServingCellID=1;
	mServingRAUID=0;
	mActuralInstantThoughput = 0.0001;
	mAveThoughput = 0.0001;




}

CUser::~CUser(void)
{
}

/***********************************切换相关函数实现****************************/
void CUser::Handover()
{
	if(RLFDetection())
	{
		mUEStatus=7;
		ResetParaAfterRLF();
	}
	switch(mUEStatus)
	{
	case 1:
		if(HOTrigger())
			mUEStatus=2;
		break;
	case 2:
		SendHOReport();
		break;
	case 3:
		HOPrePare();
		break;
	case 4:
		ReceiveHOCMD();			
		break;
	case 5:
		ConnectTargetCell();
		break;
	case 6:
		HOComplete();
		ResetParaAfterHO();//还要进行一系列清理工作
		break;
	}
}

void CUser::ResetParaAfterHO()
{
	firstR3Filter=true;
}

void CUser::ResetParaAfterRLF()
{
	mL1Timer=0;
	mL1Counter=0;
	mTrigger=false;
	mLastUpLinkSIR=0;
	mHARQCounter=0;
	mRLCCounter=0;
	mLastDownLinkSIR=0;
	mConnectTimer=0;
	mPrapareTimer=0;
	pL1RSRP.zeros();
	mTTTCounter[0]=0;
	mTTTCounter[1]=0;
	pL1RSRP.zeros();
	pSlidingWindowQout.zeros();
	mL3Timer=0;
}

double CUser::DownLinkSIR()
{
	

	return 1.0;


}

double CUser::UpLinkSIR()
{

	return 1.0;



}

double CUser::ChaseCom(double db1,double db2)
{
	double W1,W2;
	W1 = pow(10,db1/10.0);
	W2 = pow(10,db2/10.0);
	return 10 * log10(W1+W2); 
}

void CUser::MeasureRSRP()
{

}

/**
void CUser::MeasureRSRP()
{
	mL1Timer+=10;	
	if(mL1Timer>=40)
	{		
		mL1Timer=0;
		for(int i=0;i<3;i++)  //更新服务小区，及相邻的两个小区的L1层RSRP值
			mL1RSRP(i,mL1Counter)=gSignalPowerinW-pLargeScaleChannel.//更新该值
		mL1Counter++;
	}
	if(this->mL1Counter>=5)
	{
		mL1Counter=0;
		double aver[3]={0,0,0};
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<5;j++)
				aver[i]+=mL1RSRP[i][j];
			aver[i]/=5;          //层1线性滤波
		}
		if(!firstR3Filter)
		{
			for(int i=0;i<3;i++)
				mL3RSRP[i]=aver[i]*mBeta+mL3RSRP[i]*(1-mBeta);//层3滤波
		}
		else
		{
			for(int i=0;i<3;i++)
				mL3RSRP[i]=aver[i];
			firstR3Filter=false;
		}
		mTrigger=true;
	}

}
**/


/**
bool CUser::HOTrigger()
{
	this->MeasureRSRP();
	if(mTrigger)
	{
		for(int i=1;i<3;i++)
		{
			if(mL3RSRP[0]+mHOM<mL3RSRP[i])
			{
				mTTTCounter[i-1]+=200;
				if(mTTTCounter[i-1]-200>=mTTT)
				{
					mTargetCellID=BaseStation[mServingCellID].AdjanceCell[i];//怎么获得服务小区相邻小区ID
					mTrigger=false;//用于判断是否应该进行切换判决的布尔变量，设置为false
					mTTTCounter[0]=0;
					mTTTCounter[1]=0;
					return true;
				}
			}
			else
			{
				mTTTCounter[i-1]=0;						
			}	
		
		}				
	}
	mTrigger=false;
	return false;
}
**/

bool CUser::HOTrigger()
{
	mL3Timer+=10;
	//int maxValue=-10000;
	int maxValue1=-10000;//没有意义，只是足够小
	int maxValue2=-10000;
	int maxCell1=0;
	int maxRAU1=0;
	int maxCell2=1;
	int maxRAU2=0;
	if(mL3Timer>=200)
	{
		mL3Timer=0;
		pL3RSRP.zeros();
		//服务小区所有RAU RSRP值
		for(int j=0;j<gBSRAUNum;j++)
		{
			pL3RSRP(0,j)=10*log10(gSignalPowerinW)-pLargeScaleChannel(1*gBSRAUNum+j);
				
		}
		//前一个小区所有RAU RSRP值
		for(int j=0;j<gBSRAUNum;j++)
		{
			pL3RSRP(1,j)=10*log10(gSignalPowerinW)-pLargeScaleChannel(0*gBSRAUNum+j);
				
		}
		for(int j=0;j<gBSRAUNum;j++)
		{
			pL3RSRP(2,j)=10*log10(gSignalPowerinW)-pLargeScaleChannel(2*gBSRAUNum+j);
				
		}
		//找到服务RSRP最大值及其RAU索引
		for(int j=0;j<gBSRAUNum;j++)
		{
			if(pL3RSRP(0,j)>maxValue1)
			{
				maxValue1=pL3RSRP(0,j);
				maxRAU1=j;
			}	
		}
		//找到邻小区RSRP最大的小区号和RAU值
		for(int i=1;i<3;i++)
			for(j=0;j<gBSRAUNum;j++)
			{
				if(pL3RSRP(i,j)>maxValue2)
				{
				    maxValue2=pL3RSRP(i,j);
					maxCell2=i;
					maxRAU2=j;			
				}
					
			}
		if(maxValue2>maxValue1+mHOM)
		{
			if(maxCell2==1)
			{
				mTargetCellID=mServingCellID-1;
				mTargetRAUID=maxRAU2;
			}else
			{
				mTargetCellID=mServingCellID+1;
				mTargetRAUID=maxRAU2;
			}
			return true;		
		}else
		{
			mTargetRAUID=maxRAU1;
			return false;		
		
		}
	}
	return false;
}



void CUser::SendHOReport()
{
	mUEStatus=3;
}



void CUser::HOPrePare()
{
	this->mPrapareTimer+=10;
	if(mPrapareTimer>=60)
	{
		mPrapareTimer=0;
		mUEStatus=4;
	}
}

void CUser::ReceiveHOCMD()
{
	mUEStatus=5;	   
}

void CUser::ConnectTargetCell()
{
	this->mConnectTimer+=10;
	if(mConnectTimer>=30)
	{
		mConnectTimer=0;
		mUEStatus=6;		
	}
}


void CUser::HOComplete()
{
	mServingCellID=mTargetCellID;//完成服务小区的更换
	mServingRAUID=mTargetRAUID;
	mUEStatus=1;
	//mL3RSRP[0]=0;
	//mL3RSRP[1]=0;
}

bool CUser::RLFDetection()
{

	return false;

}

//*****************根据大尺度衰落信息选择RAU，并分配功率*****************
void CUser::PowerAllocation( )
{
	int temp=min(pLargeScaleChannel,mMinLargeId);//返回大尺度衰落最小的值的序号
}


//***********************************************************************
//!预编码和检测矩阵选择
void CUser::PrecodingDetectionSelection()
{
	//******************秩的自适应**************************
	double SignalPower = gSignalPowerinW;
	//double k=pow(10,-pLargeScaleChannel(mMinLargeId)/10.0);
	double sigpower = pow(10,-pLargeScaleChannel(mMinLargeId)/10.0)*SignalPower;
	
	double Segma;	
	Segma=gNoisePowerinW/sigpower;//噪声功率
		
	mEffectiveSINR = 10*log10(1/Segma);
	if(mEffectiveSINR < 3)
		mLayerNum =1;
	else
		mLayerNum =2;
	//********************************************************
	mPathNum =2;
	//mLayerNum = gLayNum;
	int Layer = mLayerNum;
	int PathNum = mPathNum;
	
	for(int subbandid = 0;subbandid < gSubBandNum;subbandid++)
	{
		pPrecodingMatrix[subbandid].set_size(gRAUAntennaNum,Layer);
		pDetectionMatrix[subbandid].set_size(Layer,gUEAntennaNum);
	}

	
	//cout<<Segma<<endl;
	//double inputnum;
	//cin>>inputnum;

	int TxNum = gRAUAntennaNum;//发射天线数
	int RxNum = gUEAntennaNum;
	//min(gBSAntennaNum,gUEAntennaNum);
	int CodeBookSize;
	int SubBandWidth = gRBperSubBand;
	int SubBandNum = gSubBandNum;
	int RBWidth = 12;

	mPrecodingMatrixID.set_size(gSubBandNum);
	
	cmat ChannelTimeDomain[2048];//只考虑一个服务扇区
	cmat ChannelFreqDomain[2048];//频域

	cmat UsedChannelFreqDomain[2048];//使用1-600的子载波
	cmat RBChannelFreqDomain[10];//10个subband，假设一个用户一个subband
	cmat CodeBook[16];//2个预编码矩阵
	ivec CodeBookIndex(10);
	cmat UsedCodeBook[10];//10个subband最后选择的码本
	cmat MSEMatrix[11][16];//每个subband每个码本的判决矩阵，16是可能的最大codebook size
	cmat DetectionMatrix[10];//每个subband的检测矩阵
	mat Det(11,16);//每个判决矩阵的行列式,不是复数,16是可能的最大codebook size
	pDelayperPath.set_size(20);

	for (int iii =0;iii<gBSRAUNum*3;iii++)
	{
		for(int jjj=0;jjj<mPathNum;jjj++)
		{
			//pChannelMatrix[i][j] = b;
			//pChannelMatrix[i][j].set_size(2,2);
			pDelayperPath[jjj] = jjj;
			//cout<<pDelayperPath[jjj]<<endl;
			pChannelMatrix[iii][jjj](0,0) = randn_c()*pow(10,-pLargeScaleChannel(iii)/20.0);
			pChannelMatrix[iii][jjj](0,1) = randn_c()*pow(10,-pLargeScaleChannel(iii)/20.0);
			pChannelMatrix[iii][jjj](0,2) = randn_c()*pow(10,-pLargeScaleChannel(iii)/20.0);
			pChannelMatrix[iii][jjj](0,3) = randn_c()*pow(10,-pLargeScaleChannel(iii)/20.0);
			pChannelMatrix[iii][jjj](1,0) = randn_c()*pow(10,-pLargeScaleChannel(iii)/20.0);
			pChannelMatrix[iii][jjj](1,1) = randn_c()*pow(10,-pLargeScaleChannel(iii)/20.0);
			pChannelMatrix[iii][jjj](1,2) = randn_c()*pow(10,-pLargeScaleChannel(iii)/20.0);
			pChannelMatrix[iii][jjj](1,3) = randn_c()*pow(10,-pLargeScaleChannel(iii)/20.0);
			
			//cout<<pChannelMatrix[iii][jjj]<<endl;



		}
	}

	if (TxNum==2)//2天线码本
	{
		if(Layer ==1)
		{
			CodeBookSize = 4;
			vec RealValue00 = "0.707107 0.707107 0.707107 0.707107";
			vec RealValue10 = "0.707107 -0.707107 0.000000 0.000000";
			vec ImagValue00 = "0.000000 0.000000 0.000000 0.000000";
			vec ImagValue10 = "0.000000 0.000000 0.707107 -0.707107";
			for(int i=0;i<CodeBookSize;i++)
			{
				CodeBook[i].set_size(2,1,0);
			}
			for(i=0;i<CodeBookSize;i++)
			{
				//column 1
				CodeBook[i](0,0).real(RealValue00(i));
				CodeBook[i](1,0).real(RealValue10(i));	
				CodeBook[i](0,0).imag(ImagValue00(i));
				CodeBook[i](1,0).imag(ImagValue10(i));
			}
		}
		if(Layer == 2)
		{
			CodeBookSize = 2;
			vec RealValue00 = "0.500000 0.500000 ";
			vec RealValue10 = "0.500000 0.000000 ";
			vec ImagValue00 = "0.000000 0.000000 ";
			vec ImagValue10 = "0.000000 0.500000 ";
			vec RealValue01 = "0.500000 0.500000 ";
			vec RealValue11 = "-0.500000 0.000000 ";
			vec ImagValue01 = "0.000000 0.000000 ";
			vec ImagValue11 = "0.000000 -0.500000 ";
			for(int i=0;i<CodeBookSize;i++)
			{
				CodeBook[i].set_size(2,2,0);
			}
			for(int i=0;i<CodeBookSize;i++)
			{
				//column 1
				CodeBook[i](0,0).real(RealValue00(i));
				CodeBook[i](1,0).real(RealValue10(i));	
				CodeBook[i](0,0).imag(ImagValue00(i));
				CodeBook[i](1,0).imag(ImagValue10(i));
				//column 2
				CodeBook[i](0,1).real(RealValue01(i));
				CodeBook[i](1,1).real(RealValue11(i));	
				CodeBook[i](0,1).imag(ImagValue01(i));
				CodeBook[i](1,1).imag(ImagValue11(i));
			}
		}
	}

	if(TxNum==4)//4天线码本
	{
		CodeBookSize = 16;
		if(Layer==1)//rank1,4行一列
		{
			vec RealValue00 = "0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 0.500000 ";//实部的值
			vec RealValue10 = "0.500000 0.000000 -0.500000 0.000000 0.353553 -0.353553 -0.353553 0.353553 0.500000 0.000000 -0.500000 0.000000 0.500000 0.500000 -0.500000 -0.500000 ";
			vec RealValue20 = "0.500000 0.500000 0.500000 -0.500000 0.000000 0.000000 0.000000 0.000000 -0.500000 0.500000 -0.500000 0.500000 0.500000 -0.500000 0.500000 -0.500000 ";
			vec RealValue30 = "0.500000 0.000000 -0.500000 0.000000 -0.353553 0.353553 0.353553 -0.353553 -0.500000 0.000000 0.500000 0.000000 -0.500000 0.500000 0.500000 -0.500000 ";
			vec ImagValue00 = "0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";//虚部的值
			vec ImagValue10 = "0.000000 0.500000 0.000000 -0.500000 0.353553 0.353553 -0.353553 -0.353553 0.000000 0.500000 0.000000 -0.500000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue20 = "0.000000 0.000000 0.000000 0.000000 0.500000 -0.500000 0.500000 -0.500000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue30 = "0.000000 -0.500000 0.000000 0.500000 0.353553 0.353553 -0.353553 -0.353553 0.000000 0.500000 0.000000 -0.500000 0.000000 0.000000 0.000000 0.000000 ";	
		
			for(int i=0;i<CodeBookSize;i++)
			{
				CodeBook[i].set_size(4,1,0);
			}
			for(int i=0;i<CodeBookSize;i++)
			{
				//column 1
				CodeBook[i](0,0).real(RealValue00(i));
				CodeBook[i](1,0).real(RealValue10(i));
				CodeBook[i](2,0).real(RealValue20(i));
				CodeBook[i](3,0).real(RealValue30(i));
				CodeBook[i](0,0).imag(ImagValue00(i));
				CodeBook[i](1,0).imag(ImagValue10(i));
				CodeBook[i](2,0).imag(ImagValue20(i));
				CodeBook[i](3,0).imag(ImagValue30(i));
			}
		}
		if(Layer == 2)
		{
			//column 1
			vec RealValue00 ="0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 ";
			vec RealValue10 ="0.353553 0.000000 -0.353553 0.000000 0.250000 -0.250000 -0.250000 0.250000 0.353553 0.000000 -0.353553 0.000000 0.353553 0.353553 -0.353553 -0.353553 ";
			vec RealValue20 ="0.353553 0.353553 0.353553 -0.353553 0.000000 0.000000 0.000000 0.000000 -0.353553 0.353553 -0.353553 0.353553 0.353553 -0.353553 0.353553 -0.353553 ";
			vec RealValue30 ="0.353553 0.000000 -0.353553 0.000000 -0.250000 0.250000 0.250000 -0.250000 -0.353553 0.000000 0.353553 0.000000 -0.353553 0.353553 0.353553 -0.353553 ";
			vec ImagValue00 ="0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue10 ="0.000000 0.353553 0.000000 -0.353553 0.250000 0.250000 -0.250000 -0.250000 0.000000 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue20 ="0.000000 0.000000 0.000000 0.000000 0.353553 -0.353553 0.353553 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue30 ="0.000000 -0.353553 0.000000 0.353553 0.250000 0.250000 -0.250000 -0.250000 0.000000 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			//column 2
			vec RealValue01 ="0.353553 0.000000 -0.353553 0.000000 -0.250000 0.250000 0.000000 0.000000 0.353553 0.000000 -0.353553 0.353553 0.353553 -0.353553 0.353553 -0.353553 ";
			vec RealValue11 ="-0.353553 0.353553 0.353553 0.353553 0.000000 0.000000 0.250000 -0.250000 0.353553 -0.353553 -0.353553 0.000000 0.353553 0.353553 0.353553 0.353553 ";
			vec RealValue21 ="-0.353553 0.000000 0.353553 0.000000 -0.250000 0.250000 0.353553 0.353553 0.353553 0.000000 0.353553 0.353553 -0.353553 0.353553 0.353553 -0.353553 ";
			vec RealValue31 ="0.353553 0.353553 -0.353553 0.353553 0.353553 0.353553 0.250000 -0.250000 0.353553 0.353553 0.353553 0.000000 0.353553 0.353553 -0.353553 -0.353553 ";
			vec ImagValue01 ="0.000000 -0.353553 0.000000 0.353553 -0.250000 -0.250000 -0.353553 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue11 ="0.000000 0.000000 0.000000 0.000000 0.353553 -0.353553 -0.250000 -0.250000 0.000000 0.000000 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue21 ="0.000000 0.353553 0.000000 0.353553 0.250000 0.250000 0.000000 0.000000 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue31 ="0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.250000 0.250000 0.000000 0.000000 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";

			for(int i=0;i<CodeBookSize;i++)
			{
				CodeBook[i].set_size(4,2,0);
			}
			for(int i=0;i<CodeBookSize;i++)
			{
				//column 1
				CodeBook[i](0,0).real(RealValue00(i));
				CodeBook[i](1,0).real(RealValue10(i));
				CodeBook[i](2,0).real(RealValue20(i));
				CodeBook[i](3,0).real(RealValue30(i));
				CodeBook[i](0,0).imag(ImagValue00(i));
				CodeBook[i](1,0).imag(ImagValue10(i));
				CodeBook[i](2,0).imag(ImagValue20(i));
				CodeBook[i](3,0).imag(ImagValue30(i));
				//column 2
				CodeBook[i](0,1).real(RealValue01(i));
				CodeBook[i](1,1).real(RealValue11(i));
				CodeBook[i](2,1).real(RealValue21(i));
				CodeBook[i](3,1).real(RealValue31(i));
				CodeBook[i](0,1).imag(ImagValue01(i));
				CodeBook[i](1,1).imag(ImagValue11(i));
				CodeBook[i](2,1).imag(ImagValue21(i));
				CodeBook[i](3,1).imag(ImagValue31(i));
			}
			
		}
		if(Layer == 3)
		{
			//column 1
			vec RealValue00 ="0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 ";
			vec RealValue10 ="0.353553 0.000000 -0.353553 0.000000 0.250000 -0.250000 -0.250000 0.250000 0.353553 0.000000 -0.353553 0.000000 0.353553 0.353553 -0.353553 -0.353553 ";
			vec RealValue20 ="0.353553 0.353553 0.353553 -0.353553 0.000000 0.000000 0.000000 0.000000 -0.353553 0.353553 -0.353553 0.353553 0.353553 -0.353553 0.353553 -0.353553 ";
			vec RealValue30 ="0.353553 0.000000 -0.353553 0.000000 -0.250000 0.250000 0.250000 -0.250000 -0.353553 0.000000 0.353553 0.000000 -0.353553 0.353553 0.353553 -0.353553 ";
			vec ImagValue00 ="0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue10 ="0.000000 0.353553 0.000000 -0.353553 0.250000 0.250000 -0.250000 -0.250000 0.000000 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue20 ="0.000000 0.000000 0.000000 0.000000 0.353553 -0.353553 0.353553 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue30 ="0.000000 -0.353553 0.000000 0.353553 0.250000 0.250000 -0.250000 -0.250000 0.000000 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			//column 2
			vec RealValue01 ="0.353553 0.000000 -0.353553 0.000000 0.250000 -0.250000 0.000000 0.000000 0.353553 0.353553 -0.353553 0.353553 0.353553 0.353553 -0.353553 -0.353553 ";
			vec RealValue11 ="0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.250000 -0.250000 0.353553 0.000000 0.353553 0.000000 0.353553 0.353553 0.353553 0.353553 ";
			vec RealValue21 ="-0.353553 0.000000 0.353553 0.000000 -0.250000 0.250000 0.353553 0.353553 0.353553 0.353553 -0.353553 0.353553 -0.353553 0.353553 0.353553 -0.353553 ";
			vec RealValue31 ="-0.353553 0.353553 -0.353553 0.353553 0.000000 0.000000 0.250000 -0.250000 0.353553 0.000000 0.353553 0.000000 0.353553 -0.353553 0.353553 -0.353553 ";
			vec ImagValue01 ="0.000000 -0.353553 0.000000 0.353553 -0.250000 -0.250000 -0.353553 0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue11 ="0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 -0.250000 -0.250000 0.000000 -0.353553 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue21 ="0.000000 0.353553 0.000000 0.353553 -0.250000 -0.250000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue31 ="0.000000 0.000000 0.000000 0.000000 -0.353553 0.353553 0.250000 0.250000 0.000000 -0.353553 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";
			//column 3
			vec RealValue02 ="0.353553 0.353553 0.353553 -0.353553 -0.250000 0.250000 0.250000 -0.250000 -0.353553 0.000000 -0.353553 0.000000 0.353553 -0.353553 0.353553 -0.353553 ";
			vec RealValue12 ="-0.353553 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.353553 -0.353553 -0.353553 -0.353553 -0.353553 0.353553 0.353553 -0.353553 ";
			vec RealValue22 ="-0.353553 0.353553 0.353553 0.353553 -0.250000 0.250000 0.250000 -0.250000 -0.353553 0.000000 0.353553 0.000000 0.353553 0.353553 0.353553 0.353553 ";
			vec RealValue32 ="0.353553 0.000000 0.353553 0.000000 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 -0.353553 -0.353553 ";
			vec ImagValue02 ="0.000000 0.000000 0.000000 0.000000 -0.250000 -0.250000 0.250000 0.250000 0.000000 -0.353553 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue12 ="0.000000 -0.353553 0.000000 -0.353553 0.353553 -0.353553 0.353553 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue22 ="0.000000 0.000000 0.000000 0.000000 0.250000 0.250000 -0.250000 -0.250000 0.000000 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue32 ="0.000000 0.353553 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";

			for(int i=0;i<CodeBookSize;i++)
			{
				CodeBook[i].set_size(4,3,0);
			}
			for(int i=0;i<CodeBookSize;i++)
			{
				//column 1
				CodeBook[i](0,0).real(RealValue00(i));
				CodeBook[i](1,0).real(RealValue10(i));
				CodeBook[i](2,0).real(RealValue20(i));
				CodeBook[i](3,0).real(RealValue30(i));
				CodeBook[i](0,0).imag(ImagValue00(i));
				CodeBook[i](1,0).imag(ImagValue10(i));
				CodeBook[i](2,0).imag(ImagValue20(i));
				CodeBook[i](3,0).imag(ImagValue30(i));
				//column 2
				CodeBook[i](0,1).real(RealValue01(i));
				CodeBook[i](1,1).real(RealValue11(i));
				CodeBook[i](2,1).real(RealValue21(i));
				CodeBook[i](3,1).real(RealValue31(i));
				CodeBook[i](0,1).imag(ImagValue01(i));
				CodeBook[i](1,1).imag(ImagValue11(i));
				CodeBook[i](2,1).imag(ImagValue21(i));
				CodeBook[i](3,1).imag(ImagValue31(i));
				//column 2
				CodeBook[i](0,2).real(RealValue02(i));
				CodeBook[i](1,2).real(RealValue12(i));
				CodeBook[i](2,2).real(RealValue22(i));
				CodeBook[i](3,2).real(RealValue32(i));
				CodeBook[i](0,2).imag(ImagValue02(i));
				CodeBook[i](1,2).imag(ImagValue12(i));
				CodeBook[i](2,2).imag(ImagValue22(i));
				CodeBook[i](3,2).imag(ImagValue32(i));
			}
		}
		if(Layer == 4)
		{
			//column 1
			vec RealValue00 ="0.353553 0.353553 0.353553 -0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 ";
			vec RealValue10 ="0.353553 0.000000 0.353553 0.000000 0.250000 -0.250000 -0.250000 0.250000 0.353553 0.000000 -0.353553 0.000000 0.353553 0.353553 0.353553 -0.353553 ";
			vec RealValue20 ="0.353553 0.353553 0.353553 0.353553 0.000000 0.000000 0.000000 0.000000 -0.353553 0.353553 -0.353553 0.353553 0.353553 -0.353553 0.353553 -0.353553 ";
			vec RealValue30 ="0.353553 0.000000 0.353553 0.000000 -0.250000 0.250000 0.250000 -0.250000 -0.353553 0.000000 0.353553 0.000000 -0.353553 0.353553 -0.353553 -0.353553 ";
			vec ImagValue00 ="0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue10 ="0.000000 0.353553 0.000000 -0.353553 0.250000 0.250000 -0.250000 -0.250000 0.000000 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue20 ="0.000000 0.000000 0.000000 0.000000 0.353553 -0.353553 0.353553 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue30 ="0.000000 -0.353553 0.000000 0.353553 0.250000 0.250000 -0.250000 -0.250000 0.000000 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			//column 2
			vec RealValue01 ="0.353553 0.000000 -0.353553 0.000000 0.250000 -0.250000 0.000000 0.000000 0.353553 0.000000 -0.353553 0.353553 0.353553 -0.353553 -0.353553 -0.353553 ";
			vec RealValue11 ="0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.250000 -0.250000 0.353553 0.353553 -0.353553 0.000000 0.353553 0.353553 0.353553 0.353553 ";
			vec RealValue21 ="-0.353553 0.000000 0.353553 0.000000 -0.250000 0.250000 0.353553 0.353553 0.353553 0.000000 0.353553 0.353553 -0.353553 0.353553 0.353553 -0.353553 ";
			vec RealValue31 ="-0.353553 0.353553 -0.353553 0.353553 0.000000 0.000000 0.250000 -0.250000 0.353553 -0.353553 0.353553 0.000000 0.353553 0.353553 0.353553 -0.353553 ";
			vec ImagValue01 ="0.000000 -0.353553 0.000000 0.353553 -0.250000 -0.250000 -0.353553 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue11 ="0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 -0.250000 -0.250000 0.000000 0.000000 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue21 ="0.000000 0.353553 0.000000 0.353553 -0.250000 -0.250000 0.000000 0.000000 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue31 ="0.000000 0.000000 0.000000 0.000000 -0.353553 0.353553 0.250000 0.250000 0.000000 0.000000 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";
			//column 
			vec RealValue02 ="0.353553 0.353553 0.353553 0.353553 0.000000 0.000000 -0.250000 0.250000 -0.353553 0.353553 -0.353553 0.000000 0.353553 0.353553 0.353553 -0.353553 ";
			vec RealValue12 ="-0.353553 0.000000 -0.353553 0.000000 -0.250000 0.250000 0.353553 0.353553 0.353553 0.000000 0.353553 0.353553 -0.353553 0.353553 -0.353553 -0.353553 ";
			vec RealValue22 ="0.353553 0.353553 0.353553 -0.353553 0.353553 0.353553 0.250000 -0.250000 0.353553 0.353553 -0.353553 0.000000 0.353553 0.353553 0.353553 0.353553 ";
			vec RealValue32 ="-0.353553 0.000000 -0.353553 0.000000 -0.250000 0.250000 0.000000 0.000000 -0.353553 0.000000 0.353553 -0.353553 0.353553 -0.353553 0.353553 -0.353553 ";
			vec ImagValue02 ="0.000000 0.000000 0.000000 0.000000 -0.353553 0.353553 0.250000 0.250000 0.000000 0.000000 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue12 ="0.000000 -0.353553 0.000000 -0.353553 0.250000 0.250000 0.000000 0.000000 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue22 ="0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.250000 0.250000 0.000000 0.000000 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue32 ="0.000000 0.353553 0.000000 0.353553 -0.250000 -0.250000 -0.353553 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			//column 3
			vec RealValue03 ="0.353553 0.000000 -0.353553 0.000000 -0.250000 0.250000 0.250000 -0.250000 -0.353553 0.000000 0.353553 0.000000 -0.353553 0.353553 0.353553 -0.353553 ";
			vec RealValue13 ="-0.353553 0.353553 -0.353553 0.353553 0.000000 0.000000 0.000000 0.000000 0.353553 -0.353553 0.353553 -0.353553 0.353553 -0.353553 0.353553 -0.353553 ";
			vec RealValue23 ="-0.353553 0.000000 0.353553 0.000000 -0.250000 0.250000 0.250000 -0.250000 -0.353553 0.000000 0.353553 0.000000 0.353553 0.353553 -0.353553 -0.353553 ";
			vec RealValue33 ="0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 0.353553 ";
			vec ImagValue03 ="0.000000 0.353553 0.000000 -0.353553 -0.250000 -0.250000 0.250000 0.250000 0.000000 -0.353553 0.000000 0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue13 ="0.000000 0.000000 0.000000 0.000000 0.353553 -0.353553 0.353553 -0.353553 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue23 ="0.000000 -0.353553 0.000000 -0.353553 0.250000 0.250000 -0.250000 -0.250000 0.000000 0.353553 0.000000 -0.353553 0.000000 0.000000 0.000000 0.000000 ";
			vec ImagValue33 ="0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 ";
			
			for(int i=0;i<CodeBookSize;i++)
			{
				CodeBook[i].set_size(4,4,0);
			}
			for(int i=0;i<CodeBookSize;i++)
			{
				//column 1
				CodeBook[i](0,0).real(RealValue00(i));
				CodeBook[i](1,0).real(RealValue10(i));
				CodeBook[i](2,0).real(RealValue20(i));
				CodeBook[i](3,0).real(RealValue30(i));
				CodeBook[i](0,0).imag(ImagValue00(i));
				CodeBook[i](1,0).imag(ImagValue10(i));
				CodeBook[i](2,0).imag(ImagValue20(i));
				CodeBook[i](3,0).imag(ImagValue30(i));
				//column 2
				CodeBook[i](0,1).real(RealValue01(i));
				CodeBook[i](1,1).real(RealValue11(i));
				CodeBook[i](2,1).real(RealValue21(i));
				CodeBook[i](3,1).real(RealValue31(i));
				CodeBook[i](0,1).imag(ImagValue01(i));
				CodeBook[i](1,1).imag(ImagValue11(i));
				CodeBook[i](2,1).imag(ImagValue21(i));
				CodeBook[i](3,1).imag(ImagValue31(i));
				//column 3
				CodeBook[i](0,2).real(RealValue02(i));
				CodeBook[i](1,2).real(RealValue12(i));
				CodeBook[i](2,2).real(RealValue22(i));
				CodeBook[i](3,2).real(RealValue32(i));
				CodeBook[i](0,2).imag(ImagValue02(i));
				CodeBook[i](1,2).imag(ImagValue12(i));
				CodeBook[i](2,2).imag(ImagValue22(i));
				CodeBook[i](3,2).imag(ImagValue32(i));
				//column 4
				CodeBook[i](0,3).real(RealValue03(i));
				CodeBook[i](1,3).real(RealValue13(i));
				CodeBook[i](2,3).real(RealValue23(i));
				CodeBook[i](3,3).real(RealValue33(i));
				CodeBook[i](0,3).imag(ImagValue03(i));
				CodeBook[i](1,3).imag(ImagValue13(i));
				CodeBook[i](2,3).imag(ImagValue23(i));
				CodeBook[i](3,3).imag(ImagValue33(i));
			}			
		}
	}
	//以上为码本产生
	//初始化信道向量
	for(int i=0;i<gFFTLength;i++)
	{
		ChannelTimeDomain[i].set_size(RxNum,TxNum);
		ChannelTimeDomain[i].zeros();//置零
	}
	for(int i=0;i<gFFTLength;i++)//后面的0都没用
	{
		ChannelFreqDomain[i].set_size(RxNum,TxNum);
		ChannelFreqDomain[i].zeros();//置零
	}
	//产生6径信道
	for(int i=0;i<PathNum;i++)
	{
		ChannelTimeDomain[i] = pChannelMatrix[mMinLargeId][i];//randn_c(RxNum,TxNum);//矩阵赋给矩阵
	}	
	//信道fft
	for(int i=0;i<RxNum;i++)	
	{
		for(int j=0;j<TxNum;j++)
		{
			cvec Temp1(gFFTLength);
			cvec Temp2(gFFTLength);
			for(int k=0;k<gFFTLength;k++)
				Temp1(k) = ChannelTimeDomain[k](i,j);
			Temp2 = fft(Temp1,gFFTLength);
			for(int k=0;k<gFFTLength;k++)
				ChannelFreqDomain[k](i,j) = Temp2(k);
			//cout<<mUserID<<" "<<Temp2<<endl;
		}
	}
	//取出使用的子载波,使用1-600子载波，共600个，10个subband,每个子带5个RB
	for(int j=0,i=1;i<SubBandNum*SubBandWidth*RBWidth+1;i++,j++)
	{
		UsedChannelFreqDomain[j] = ChannelFreqDomain[i];
	}
	//信道平均到每个子带上？？？
	for(int i=0;i<SubBandNum;i++)
	{
		cmat sum;
		sum.set_size(RxNum,TxNum,0);
		sum.zeros();
		for(int j=0;j<RBWidth*SubBandWidth;j++)
		{
			sum = sum + ChannelFreqDomain[i*RBWidth*SubBandWidth + j];
		}
		RBChannelFreqDomain[i] = sum/(RBWidth*SubBandWidth);
	}
	//预编码矩阵决定及检测矩阵计算
	for(int i=0;i<SubBandNum;i++)//10个子带
	{
		cmat ChannelTemp;//等效信道矩阵
		for(int j=0;j<CodeBookSize;j++)
		{		
			ChannelTemp = RBChannelFreqDomain[i] * CodeBook[j];
			MSEMatrix[i][j] = itpp::inv(itpp::hermitian_transpose(ChannelTemp)*ChannelTemp + itpp::eye_c(Layer));//公式不完整，还有发射功率和噪声功率
			Det(i,j) = itpp::det(MSEMatrix[i][j]).real();//虚部=0
		}
		//当前RB的各个DET求最小值，注意Det的列数大于码本size，后边的值很小，itpp提供的函数会选出后边没定义的最小值
		//CodeBookIndex(i) = itpp::min_index(Det.get_row(i));自己写一个
		CodeBookIndex(i) = 0;
		for(int j=1;j<CodeBookSize;j++)
		{				
			if(Det(i,CodeBookIndex(i)) > Det(i,j))
				CodeBookIndex(i) = j;
		}
		UsedCodeBook[i] = CodeBook[CodeBookIndex(i)];//最佳码本
		//计算检测矩阵
		ChannelTemp = RBChannelFreqDomain[i] * UsedCodeBook[i];//已确定的等效信道矩阵
		DetectionMatrix[i] = itpp::inv(itpp::hermitian_transpose(ChannelTemp)*ChannelTemp
							 + Segma * itpp::eye_c(Layer))*itpp::hermitian_transpose(ChannelTemp);//Segma为噪声功率						 						 
	}
	
	//写入用户信息
	for(int i=0;i<SubBandNum;i++)
	{
		mPrecodingMatrixID[i] = CodeBookIndex(i);
		pPrecodingMatrix[i] = UsedCodeBook[i];
		pDetectionMatrix[i] = DetectionMatrix[i];
	}

	//输出
	//for(i=0;i<SubBandNum;i++)
	//{
	//	cout << "RB Number             "<< i << endl;
	//	cout << "所选码本序号          "<< mPrecodingMatrixID << endl;//输出10个RB选择码本的序号
	//	cout << "precode matrix chosed " << endl << pPrecodingMatrix[i] << endl;//输出10个RB选择的预编码矩阵
	//	cout << "Detection Matrix      " << endl << pDetectionMatrix[i] << endl; //输出10个RB的检测矩阵
	//	cout << "**********************"<<endl;
	//}
	//cin >> Finish;

}



//!计算每个子载波受到的相邻5对子载波间干扰
double CUser::interfromothercarriers(int subcarrierindex,int layid)
{
	int startindex,endindex;		//表示当前子载波前后相邻的5对子载波的序号

	if(subcarrierindex<=5)
	{
		startindex=0;
		endindex=subcarrierindex+5;
	}
	else if(subcarrierindex>=gSubBandNum*gRBperSubBand*12-5)
	{
		startindex=subcarrierindex-5;
		endindex=gSubBandNum*gRBperSubBand*12;
	}
	else
	{
		startindex=subcarrierindex-5;
		endindex=subcarrierindex+5;
	}
	
	cvec temp(gFFTLength);

	for(int i = 0;i<gUEAntennaNum;i++)
	{
		for(int j = 0;j<gRAUAntennaNum;j++)
		{
			FFTtemp[i][j].set_size(gFFTLength);
			temp.zeros();
			int pathbase = 0;
			for(int pathid = 0;pathid<gFFTLength;pathid++)
			{
				if( pathid == (int)(pDelayperPath[pathbase]))
				{
					temp[pathid] = pChannelMatrix[mMinLargeId][pathbase](i,j);	//mMinLargeId表示当前为用户服务的RAU的序号
					pathbase ++;
					if(pathbase == mPathNum)
						break;
				}
			}		
			FFTtemp[i][j] = fft(temp,gFFTLength);	
		}
	}
	double totalinterferencepower=0, interferencepower;

	cmat ChannelMatrix(gUEAntennaNum,gRAUAntennaNum);
	cmat PrecodingColumn(gRAUAntennaNum,1),MMSEMatrix(1,gUEAntennaNum);

	
	for(int i=startindex;i<endindex;i++)
	{
		int subbandindex = ceil_i(i/gRBperSubBand/12);
		PrecodingColumn = pPrecodingMatrix[subbandindex].get_col(layid);
		int subbandindex1 = ceil_i(subcarrierindex/gRBperSubBand/12);
		MMSEMatrix = pDetectionMatrix[subbandindex1].get_row(layid).transpose();
		
		if(i!=subcarrierindex)
		{
			for(int ii = 0;ii<gUEAntennaNum;ii++)
			{
				for(int jj = 0;jj<gRAUAntennaNum;jj++)
					ChannelMatrix(ii,jj) = FFTtemp[ii][jj](i);
			}
			mat PowerFactor(gRAUAntennaNum,1);
			PowerFactor.ones();
			cmat resultpower(1,1);
			resultpower = MMSEMatrix*ChannelMatrix*PowerFactor;
			interferencepower = pow(abs(resultpower(0,0)),2);
			totalinterferencepower += interferencepower;
		}
	}

	
	return totalinterferencepower;

}

//*************!SINR估计**************************************************
void CUser::SINREst()
{
	//*****************************Memory Allocation***********************
	int LayerNum = mLayerNum;
	pSubCarrierSINR.set_size(gSubBandNum*gRBperSubBand*12,LayerNum);
	pSubCarrierSignalPower.set_size(gSubBandNum*gRBperSubBand*12,LayerNum);
	pSubCarrierInterefence.set_size(gSubBandNum*gRBperSubBand*12,LayerNum);
	pSubCarrierInterefence.zeros();
	pChannelEstErr.set_size(gFFTLength,LayerNum);
	pChannelEstErr.zeros();

	//***************Precoding and Detection Matrix Initialization*********

	int RBNum = gSubBandNum*gRBperSubBand;
	int RENum = RBNum *12;

	cmat ChannelMatrix(gUEAntennaNum,gRAUAntennaNum);
	cmat PrecodingColumn(gRAUAntennaNum,1),MMSEMatrix(1,gUEAntennaNum);
	
	
	//*********************计算每个子载波的功率以及子载波间干扰************
	cvec temp(gFFTLength);

	for(int i = 0;i<gUEAntennaNum;i++)
	{
		for(int j = 0;j<gRAUAntennaNum;j++)
		{
			FFTtemp[i][j].set_size(gFFTLength);
			temp.zeros();
			int pathbase = 0;
			for(int pathid = 0;pathid<gFFTLength;pathid++)
			{
				if( pathid == (int)(pDelayperPath[pathbase]))
				{
					temp[pathid] =  pChannelMatrix[mMinLargeId][pathbase](i,j);	//mMinLargeId表示当前为用户服务的RAU的序号
					pathbase ++;
					if(pathbase == mPathNum)
						break;
				}
			}		
			FFTtemp[i][j] = fft(temp,gFFTLength);	
		}
	}

	double totalinterferencepower;		//每个子载波受到的其它子载波间干扰的总和	
	
	for(int layid = 0;layid<LayerNum;layid++)
	{
		for(int subbandindex= 0;subbandindex<gSubBandNum;subbandindex++)
		{
			//*************************每个子带的预编码矩阵************************
			PrecodingColumn = pPrecodingMatrix[subbandindex].get_col(layid);
			MMSEMatrix = pDetectionMatrix[subbandindex].get_row(layid).transpose();
			/*cout<<PrecodingColumn<<endl<<MMSEMatrix<<endl<<pDetectionMatrix[subbandindex].get_row(layid)<<endl<< pDetectionMatrix[subbandindex]<<endl;*/

			for(int i = 0;i<gRBperSubBand;i++)
			{
				for(int j = 0;j<12;j++)
				{
					//*******每一个子载波的信道矩阵******************
					for(int ii = 0;ii<gUEAntennaNum;ii++)
					{
						for(int jj = 0;jj<gRAUAntennaNum;jj++)
							ChannelMatrix(ii,jj) = FFTtemp[ii][jj] (subbandindex*gRBperSubBand*12+i*12 + j);
					}
					//*******每一个子载波的有用信号功率**************
					pSubCarrierSignalPower(subbandindex*gRBperSubBand*12+i*12 + j,layid) = pow(abs(det(MMSEMatrix*ChannelMatrix*PrecodingColumn)),2);//*abs(MMSEMatrix*ChannelMatrix*PrecodingColumn);
					//*******每一个子载波的干扰功率******************
					totalinterferencepower = interfromothercarriers(subbandindex*gRBperSubBand*12+i*12 + j,layid);									
					pSubCarrierInterefence(subbandindex*gRBperSubBand*12+i*12 + j,layid) = totalinterferencepower/float(gRAUAntennaNum);//*abs(MMSEMatrix*ChannelMatrix*PrecodingColumn);
				}
			}
		}
	}
	
	for(int layid = 0;layid<LayerNum;layid++)
	{
		for(int subbandindex= 0;subbandindex<gSubBandNum;subbandindex++)
		{
			double SignalPower = gSignalPowerinW;
			double awgnnoisepower = gNoisePowerinW;
			MMSEMatrix = pDetectionMatrix[subbandindex].get_row(layid).transpose();
			//mChannelEstErr = pow(10,((-2-mEffectiveSINR)/5.0))*pow(10,-pLargeScaleChannel(mMinLargeId)/10.0)*SignalPower;
			double noisepower =  abs(det(MMSEMatrix*((awgnnoisepower)*eye_c(gUEAntennaNum))*MMSEMatrix.hermitian_transpose()));

			for(int i = 0;i<gRBperSubBand;i++)
			{
				for(int j = 0;j<12;j++)
				{
					pSubCarrierSINR(subbandindex*gRBperSubBand*12+i*12 + j,layid) = pSubCarrierSignalPower(subbandindex*gRBperSubBand*12+i*12 + j,layid) /(pSubCarrierInterefence(subbandindex*gRBperSubBand*12+i*12 + j,layid)+noisepower);//+NoisePower(REIndex));
					pChannelEstErr(subbandindex*gRBperSubBand*12+i*12 + j,layid) = pow(10,((-2-10*log10(pSubCarrierSINR(subbandindex*gRBperSubBand*12+i*12 + j,layid)))/5.0))*pow(10,-pLargeScaleChannel(mMinLargeId)/10.0)/mLayerNum*gSignalPowerinW;
					double channelesterr = abs(det(MMSEMatrix*((pChannelEstErr(subbandindex*gRBperSubBand*12+i*12 + j,layid))*eye_c(gUEAntennaNum))*MMSEMatrix.hermitian_transpose()));
					pSubCarrierSINR(subbandindex*gRBperSubBand*12+i*12 + j,layid) = pSubCarrierSignalPower(subbandindex*gRBperSubBand*12+i*12 + j,layid) /(pSubCarrierInterefence(subbandindex*gRBperSubBand*12+i*12 + j,layid)+noisepower+channelesterr);//+NoisePower(REIndex));
					/*	cout<<10*log10(pSubCarrierSINR(subbandindex*gRBperSubBand*12+i*12 + j,layid))<<endl<<"mEffectiveSINR = "<<mEffectiveSINR<<endl;*/
				}
			}
		}
	}

}

//!************************SINR映射**************************************
double CUser::SINRMapping(int RENum,int InitialIndex,double CodeRate,int ModType,double Beta,int LayerID)
{
	double temp = 0;
for(int i = InitialIndex;i<RENum+InitialIndex;i++)
{
	temp += exp(-pSubCarrierSINR(i,LayerID)/Beta);
}
//cout<<"The "<<mUserID<<"th User SINR:"<<pSubCarrierSINR<<endl;
//cout<<"temp = "<<temp<<"SINR = "<<-Beta*log(1.0/RENum*temp)<<endl;
return 10*log10(-Beta*log(1.0/RENum*temp));
}


//**********************MCS选择******************************************
void CUser::MCSSelection()
{
	int mcsnum = 15;
	pCodingRate.set_size(gSubBandNum,mLayerNum);
	pRequiredThoughput.set_size(gSubBandNum,mLayerNum);
	pModType.set_size(gSubBandNum,mLayerNum);
	pBeta.set_size(gSubBandNum,mLayerNum);
	mat MCSMatrix("0 0.333 1.49; 0 0.5 1.57;0 0.667 1.69;1 0.33 3.36;0 0.75 1.69; 0 0.8 1.65;2 0.33 9.21;1 0.5 4.56;1 0.667 6.42;1 0.75 7.33;  2 0.5 13.76;1 0.8 7.68;2 0.667 20.57; 2 0.75 25.16;2 0.8 28.38");	
	for( int layid = 0;layid<mLayerNum;layid++)
	{
		for( int subbandindex = 0;subbandindex<gSubBandNum;subbandindex++)
		{
			pCodingRate(subbandindex,layid) = 0.333;
			pModType(subbandindex,layid) = 0;
			pRequiredThoughput(subbandindex,layid) =0;		
			pBeta(subbandindex,layid) = 1.49;
			for( int mcsid = mcsnum-1;mcsid >=0;mcsid--)
			{
				double SINR = SINRMapping(gRBperSubBand*12,subbandindex*gRBperSubBand*12,MCSMatrix(mcsid,1),MCSMatrix(mcsid,0),MCSMatrix(mcsid,2),layid);

				if(MCSDetermination(MCSMatrix(mcsid,1),MCSMatrix(mcsid,0),SINR)==1)
				{
					pCodingRate(subbandindex,layid) = MCSMatrix(mcsid,1);
					pModType(subbandindex,layid) = MCSMatrix(mcsid,0);
					pBeta(subbandindex,layid) = MCSMatrix(mcsid,2);
					if(pModType(subbandindex,layid) ==0)
						pRequiredThoughput(subbandindex,layid) = 2*pCodingRate(subbandindex,layid);
					else if(pModType(subbandindex,layid) ==1)
						pRequiredThoughput(subbandindex,layid)  = 4*pCodingRate(subbandindex,layid);
					else if(pModType(subbandindex,layid) ==2)
						pRequiredThoughput(subbandindex,layid) = 6*pCodingRate(subbandindex,layid);
					//mcsid = -2;
					break;
				}
			}

		}
	}
}

bool CUser::MCSDetermination(double CodeRate, int ModType, double SINR)
{
	mat cut_off("-1.5,0.9,2.9,4.1,4.9;3.5,6.3,9.15,10.1,11;6.7,10.1,12.5,14.0,14.5");//!BLER为0.1的截断值，SINR大于相应的值时，满足BLER小于0.1，否则不满足	
	double CodingRate[5] = { 0.333333, 0.5, 0.666666, 0.75, 0.8 };
	 if ( SINR > cut_off(CodeRate, ModType)) 
		 return true;
	 else 	
		 return false;
}



void CUser::InstantThrougputComputing()
{
	for(int subbandIndex=0;subbandIndex<gSubBandNum;subbandIndex++)
		for( int layid = 0;layid<mLayerNum;layid++)
		{		
			mActuralInstantThoughput+=pRequiredThoughput(subbandIndex,layid);		
		}

}
//改到main函数里


