#pragma once


//线性小区网络参数配置
extern int gBSNum = 30;								//基站数目
extern int gUEAntennaNum = 2;						//移动台天线数目
extern double gSystemCentreFrequency = 0.8;			//中心频率
extern int gUserNumPerCell = 1;						//每个小区内的移动台数目
extern double gUEAntennaSpacing=0.5;                   //用户中天线间隔0.5
extern int gInterSiteDistance = 3000;               //基站间距离(m)
extern int gBSToRailDistance = 30;                  //基站到铁轨的垂直距离 (单位是米)
extern int gServiceType;                            //业务类型 
extern int gFFTLength = 1024;						//FFT个数
extern int gSenario = 6;							//从以下6种仿真场景选择一种：室内热点(in-door-hotspot)、市区微小区(urban-micro)、市区宏小区(urban-macro)、郊区宏小区(rural-macro)）、U型、隧道
extern int gUserDistribution = 1;					//用户在小区内的分布模型（均匀分布、非均匀分布）
extern int gShadowFadingSD;							//阴影衰落标准差/dB
extern int gMaxHARQNumber;							//最大HARQ重传次数
extern int gTTIduration = 1;						//每个TTI持续时间/ms
extern int gSubBandNum = 10;						//子带个数.
extern int gRBperSubBand = 5;						//每个子带的RB个数
extern int gSimulationDuration = 600;	     		//仿真时长
extern int gSpeedOfTrain = 100;						//列车运行速度(m/s)
extern double gTS = 1.0/(15000*gFFTLength);

extern int gBSRAUNum = 4;							//每个基站中RAU个数
extern int gRAUAntennaNum = 4;						//每个RAU中天线个数
extern double gRAUSpacing = 500;					//RAU间隔
extern double gRAUAntennaSpacing=0.5;				//RAU中天线间隔0.5,4,10(lambta)
extern double gBandWidth=10;                        // 带宽
extern double gNoisePowerindB = -174.0;
extern double gNoisePowerinW = pow(10,gNoisePowerindB/10.0)*15;
extern double gSignalPowerinW =5/float(gFFTLength);

